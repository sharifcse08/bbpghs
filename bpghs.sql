-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2014 at 07:26 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bpghs`
--

-- --------------------------------------------------------

--
-- Table structure for table `criteria`
--

CREATE TABLE IF NOT EXISTS `criteria` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `criteria`
--

INSERT INTO `criteria` (`id`, `name`, `type`, `description`) VALUES
(2, 'test3', 'income', 'testtestst'),
(3, 'test', 'income', 'asdfasdf'),
(5, 'expense1', 'expense', 'asdfasdf'),
(6, 'expense2', 'expense', 'afasd');

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE IF NOT EXISTS `expense` (
  `expense_id` int(15) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `time` date NOT NULL,
  `amount` float NOT NULL,
  `financial_year` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `criteria_id` int(11) NOT NULL,
  `voucher_id` int(15) DEFAULT NULL,
  `receiver_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `approver_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modified_id` int(11) DEFAULT NULL,
  `modification_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`expense_id`),
  KEY `criteria_idx` (`criteria_id`),
  KEY `user_idx` (`modified_id`),
  KEY `voucher_idx` (`voucher_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`expense_id`, `description`, `time`, `amount`, `financial_year`, `criteria_id`, `voucher_id`, `receiver_name`, `approver_name`, `modified_id`, `modification_time`) VALUES
(1, 'adf', '2014-05-18', 34234, '2013-2014', 5, 18, 'test doctor test doctor', 'asdf ADFASDF', 1, '2014-05-18 12:21:04'),
(2, 'adasd', '2014-05-26', 33, '2013-2014', 6, 20, 'Md Shariful Islam saful', 'Md Shariful Islam saful', 1, '2014-05-26 10:15:07');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `file_id` int(15) NOT NULL AUTO_INCREMENT,
  `voucher_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_path` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `voucher_name`, `type`, `number`, `file_path`, `time_updated`) VALUES
(1, 'Capture2.PNG', 'income', '32423', 'IncomeVoucher/Capture2.PNG', '2014-04-27 19:57:56'),
(2, 'ubuntu8.PNG', 'income', '3', 'IncomeVoucher/ubuntu8.PNG', '2014-04-27 20:02:50'),
(4, 'BURUJ_BAGAN_PILOT_GIRLS_SCHOOL2014_--Edit_User_2014-04-22_14-07-231.png', 'income', '3', 'IncomeVoucher/BURUJ_BAGAN_PILOT_GIRLS_SCHOOL2014_--Edit_User_2014-04-22_14-07-231.png', '2014-04-27 20:11:19'),
(7, 'Capture2.PNG', 'expense', '14', 'ExpensesVoucher/Capture2.PNG', '2014-04-27 20:39:23'),
(8, '1614142066-Photo_on_5-3-14_at_6.47_PM_.jpg', 'income', 'asdfasd', 'IncomeVoucher/1614142066-Photo_on_5-3-14_at_6.47_PM_.jpg', '2014-05-18 10:33:29'),
(9, '1614142066-Photo_on_5-3-14_at_6.47_PM_1.jpg', 'income', 'aasdf', 'IncomeVoucher/1614142066-Photo_on_5-3-14_at_6.47_PM_1.jpg', '2014-05-18 10:34:04'),
(10, '10169371_725905434118020_1952475775_n.jpg', 'income', 'asdf', 'IncomeVoucher/10169371_725905434118020_1952475775_n.jpg', '2014-05-18 10:34:52'),
(11, '10169371_725905434118020_1952475775_n1.jpg', 'income', '12312', 'IncomeVoucher/10169371_725905434118020_1952475775_n1.jpg', '2014-05-18 10:35:45'),
(12, '10169371_725905434118020_1952475775_n2.jpg', 'income', '32234', 'IncomeVoucher/10169371_725905434118020_1952475775_n2.jpg', '2014-05-18 10:36:29'),
(13, '1614142066-Photo_on_5-3-14_at_6.47_PM_.jpg', 'expense', '3423', 'ExpensesVoucher/1614142066-Photo_on_5-3-14_at_6.47_PM_.jpg', '2014-05-18 12:21:04'),
(14, '10169371_725905434118020_1952475775_n3.jpg', 'income', 'asdasd', 'IncomeVoucher/10169371_725905434118020_1952475775_n3.jpg', '2014-05-18 12:45:55'),
(15, '10169371_725905434118020_1952475775_n4.jpg', 'income', 'asdfa', 'IncomeVoucher/10169371_725905434118020_1952475775_n4.jpg', '2014-05-18 12:47:50'),
(16, '10169371_725905434118020_1952475775_n5.jpg', 'income', 'adf', 'IncomeVoucher/10169371_725905434118020_1952475775_n5.jpg', '2014-05-18 12:49:17'),
(17, '10169371_725905434118020_1952475775_n6.jpg', 'income', '233', 'IncomeVoucher/10169371_725905434118020_1952475775_n6.jpg', '2014-05-18 12:49:46'),
(18, '10169371_725905434118020_1952475775_n7.jpg', 'income', '233', 'IncomeVoucher/10169371_725905434118020_1952475775_n7.jpg', '2014-05-18 12:50:28'),
(19, '10264322_10151950580531353_1436272464894605152_n.jpg', 'income', 'asdf', 'IncomeVoucher/10264322_10151950580531353_1436272464894605152_n.jpg', '2014-05-20 12:51:15'),
(21, 'Penguins.jpg', 'income', 'asdas222', 'IncomeVoucher/Penguins.jpg', '2014-05-29 04:51:17');

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE IF NOT EXISTS `income` (
  `income_id` int(15) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `time` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `amount` float NOT NULL,
  `financial_year` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `criteria_id` int(11) NOT NULL,
  `voucher_id` int(15) DEFAULT NULL,
  `receiver_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modified_id` int(11) DEFAULT NULL,
  `modification_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`income_id`),
  KEY `criteria_idx` (`criteria_id`),
  KEY `user_idx` (`modified_id`),
  KEY `file_idx` (`voucher_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`income_id`, `description`, `time`, `amount`, `financial_year`, `criteria_id`, `voucher_id`, `receiver_name`, `modified_id`, `modification_time`) VALUES
(4, 'dasdf', '2014-02-04', 500, '2013-2014', 3, 10, 'asdfasdf', 1, '2014-05-18 10:34:52'),
(5, 'fasdf', '2013-12-02', 300, '2013-2014', 3, 11, 'asdfad', 1, '2014-05-18 10:35:45'),
(10, 'adfa', '2014-05-13', 3, '2013-2014', 3, 17, 'Md Shariful Islam saful', 1, '2014-05-18 12:49:46'),
(12, 'asd', '2014-05-29', 2423, '2013-2014', 2, 21, 'Md Shariful Islam saful', 1, '2014-05-29 04:51:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL DEFAULT 'male',
  `contact_number` varchar(50) NOT NULL,
  `address` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `member_since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  KEY `role` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `designation`, `email`, `password`, `gender`, `contact_number`, `address`, `role`, `member_since`, `is_active`) VALUES
(1, 'Md Shariful Islam', 'saful', '', 'sharif.cse.08@gmail.com', 'test', 'male', '01745192550', '', 1, '2014-04-15 12:24:14', 1),
(3, 'test doctor', 'test doctor', '', 'doctor@doctor.com', 'test', 'male', '', '', 2, '2014-04-15 15:16:39', 1),
(4, 'test patient', 'test patient', '', 'patient@patient.com', 'test', 'female', '1044444444', 'dfsdf', 2, '2014-04-15 15:16:39', 0),
(5, 'test operat', 'test operat', '', 'opterat@operat.com', 'test', 'female', '', '', 1, '2014-04-15 15:17:50', 1),
(7, 'test', 'test', 'bar', 'test@test.com', 'test', 'male', '14252422232', '', 3, '2014-04-15 18:07:01', 0),
(8, 'asdf', 'ADFASDF', '', 'asdf@asda.cod', 'a', 'male', 'asdf', 'adf', 2, '2014-04-15 18:20:59', 1),
(9, 'asdf', 'ADFASDF', '', 'adsdf@asda.cod', 'a', 'male', '', '', 1, '2014-04-15 18:22:43', 1),
(10, 'asdf', 'ADFASDF', '', 'adfsdf@asda.cod', 'a', 'male', '', '', 1, '2014-04-15 18:25:03', 1),
(11, 'asdf', 'ADFASDF', '', 'adfsddf@asda.cod', 'a', 'male', '', '', 1, '2014-04-15 18:25:54', 0),
(12, 'Md Shariful ISlam', 'tset', '', 'sharif.dcse.08@gmail.com', 'asdf', 'male', '', '', 1, '2014-04-18 10:46:05', 0),
(13, 'Md Shariful ISlam', 'asdf', '', 'sasdharif.cse.08@gmail.com', 'asdf', 'male', '', '', 1, '2014-04-18 10:50:46', 0),
(14, 'new', 'user', 'test2', 'new@new.com', 'test', 'male', '234234', '', 1, '2014-05-18 11:41:35', 0),
(15, 'asdfasdas', 'dfasdf', 'test1', 'sadfasd@asdf.com', 'test', 'male', '01745192550', '', 1, '2014-05-18 12:41:15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(200) NOT NULL,
  `role_id` int(11) NOT NULL,
  `activity` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_role_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_role_id`, `role_name`, `role_id`, `activity`) VALUES
(1, 'Adminstrator', 1, 1),
(2, 'Teacher', 2, 1),
(3, 'operator', 3, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

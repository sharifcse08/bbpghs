<!-- Start: FOOTER -->
<footer>
    <!--div class="container">
        <div class="row">

        </div>
    </div-->
    <!--hr class="footer-divider"-->
    <div class="container">
        <p>
            <a target="_blank" href="http://dhrubokinfotech.com/" style="color: white"> &copy; <?php echo COPYRIGHT ;?></a>
        </p>
    </div>
</footer>

<?php
$fyear=DATE('Y');

if(date('m')>6)
    $fyear=$fyear+1;
else
    $fyear=$fyear;

$fdate=($fyear-1)."-07-01";
$now = time(); // or your date as well
$fdate = strtotime($fdate);
$datediff = $now - $fdate;
$days= floor($datediff/(60*60*24));

//echo $days;
?>


<!-- End: FOOTER -->
<script src="<?php echo base_url() ;?>assets/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/boot-business.js"></script>

<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('.input-daterange').datepicker({
            todayBtn: "linked",
            format: "yyyy-mm-dd"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#example1').datepicker({
            format: "yyyy-mm-dd",
            startDate: '-<?php echo $days;?>d',
            endDate: '+0d'
        });

    });
</script>
</body>
</html>

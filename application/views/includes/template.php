<?php include('includes/header.php') ?>
    <!-- Start: MAIN CONTENT -->
    <div class="content">
        <div class="container">
            <div class="page-header">
                <h1>Sign in to SMS-Category</h1>
            </div>
            <div class="row">
                <div class="span6 offset3">
                    <h4 class="widget-header"><i class="icon-lock"></i> Sign in to SMS-Category</h4>
                    <div class="widget-body">
                        <div class="center-align">

                            </span>

                            <form method="post"  action="#" class="form-horizontal form-signin-signup">
                                <input type="text" name="username"  value="" placeholder="Email">
                                <input type="password" name="password" placeholder="Password">
                                <div class="remember-me">
                                    <div class="pull-left">
                                        <label class="checkbox">
                                            <input type="checkbox"> Remember me
                                        </label>
                                    </div>
                                    <div class="pull-right">
                                        <a href="#">Forgot password?</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <input type="submit" value="Signin" class="btn btn-primary btn-large">
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End: MAIN CONTENT -->
<?php include('includes/footer.php') ?>
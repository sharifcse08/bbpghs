<!-- Start: HEADER -->

<!-- Docs master nav -->
<header class="navbar navbar-inverse navbar-fixed-top" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand active-link" href="<?php echo base_url()."login/"; ?>"><?php echo LOGOTEXT; ?></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="" ><a  href="<?php echo base_url() ?>login/"><?php echo SIGNIN ;?></a></li>
            </ul>
        </div>
    </div>
</header>


<!-- End: HEADER -->
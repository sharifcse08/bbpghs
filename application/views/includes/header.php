<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bootbusiness | Short description about company">
    <meta name="author" content="Your name">
    <title><?php if(isset($title)) echo $title;?></title>
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap CSS and bootstrap datepicker CSS used for styling the demo pages-->
    <link rel="stylesheet" href="<?php echo base_url() ;?>assets/css/datepicker.css">
    <!-- Bootstrap responsive -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
    <!-- Font awesome - iconic font with IE7 support -->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome-ie7.css" rel="stylesheet">
    <!-- Bootbusiness theme -->
    <link href="<?php echo base_url(); ?>assets/css/boot-business.css" rel="stylesheet">


    <!--  datatable -->
    <link href="<?php echo base_url(); ?>assets/dataTable/css/jquery.dataTables.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/dataTable/css/customDataTable.css" media="screen" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>

    <!-- Datatable Loading...-->
    <script src="<?php echo base_url(); ?>assets/dataTable/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <!--script src="<?php echo base_url(); ?>assets/dataTable/js/paging.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dataTable/js/customDataTable.js" type="text/javascript"></script-->

    <script>
        $(document).ready(function() {
            $('#apptTable').dataTable();
        } );
    </script>

    <style>
        .navbar {
            background-color: #778899;
            margin: -1px -1px 10px -1px;
        }

        .navbar-default .navbar-brand {
            color: white;
        }
        .navbar .nav > li > a {
            border-radius: 3px;
            color: #FFFFFF;
            font-size: 17px;
            font-weight: bold;
            text-shadow: none;
        }

    </style>

</head>
<body>

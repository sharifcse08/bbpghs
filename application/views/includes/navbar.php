<!-- Start: HEADER -->
<?php

$user_data=$this->session->all_userdata();

?>

<!-- Docs master nav -->
<header class="navbar navbar-default navbar-fixed-top" role="banner" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>"><?php echo LOGOTEXT; ?></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav left">
                <li>
                    <a style="margin-left: 160px"><?php if(isset($header)) echo $header; ?> </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a>স্বাগতম<span style=""> <?php echo " ".$user_data['first_name']. " ".$user_data['last_name']?></span> </a></li>
                <li><a class="<?php if(isset($menu)&& $menu=="signout") echo "active" ; ?> "href="<?php echo base_url() ?>logout/"><?php echo SIGNOUT ;?></a></li>
            </ul>
        </div>
    </div>
</header>

<!-- End: HEADER -->
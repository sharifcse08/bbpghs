<?php include('includes/header.php') ?>
    <header class="navbar navbar-default navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>"><?php echo LOGOTEXT; ?></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a>স্বাগতম ইউজার </a></li>
                    <li><a class="<?php if(isset($menu)&& $menu=="signout") echo "active" ; ?> "href="<?php echo base_url() ?>logout/"><?php echo SIGNOUT ;?></a></li>
                </ul>
            </div>
        </div>
    </header>
<script type="text/javascript">
    $(document).ready(function(){
        $('.list-group-item').hover(function(){
            $(this).css('background-color','#428BCA');
            $(this).css('color','#FFFFFF')
        }, function(){
            $(this).css('background-color','#FFFFFF');
            $(this).css('color','#333333');
        });
    });

</script>

    <!-- Start: MAIN CONTENT -->
    <div class="content">
        <div class="container">
            <div class="row">

                <div class="offset1 col-lg-4" style="min-height: 250px">
                    <div class="well well-sm">
                        <div class="panel panel-info">
                            <div class="panel-heading ">
                                <h3 class="panel-title  center-align">ব্যবহারকারী</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <a href="<?php echo base_url() ?>admin/Create/" class="list-group-item  " >নতুন ব্যবহারকারী </a>
                                    <a href="<?php echo base_url() ?>admin/ViewAllUser/" class="list-group-item">সকল ব্যবহারকারীর তথ্য </a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="offset1 col-lg-4" style="min-height: 250px">
                    <div class="well well-sm">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title center-align">খাত সমূহ</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <a  href="<?php echo base_url() ?>operator/AddCriteria/" class="list-group-item">নতুন খাত </a>
                                    <a href="<?php echo base_url()?>operator/ViewAllCriteria/" class="list-group-item">সকল খাতের বিবরণ </a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="offset1 col-lg-4" style="min-height: 250px">
                    <div class="well well-sm">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title center-align">আয়</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <a  href="<?php echo base_url() ?>operator/AddIncome/" class="list-group-item">নতুন  আয় </a>
                                    <a href="<?php echo base_url()."operator/ViewAllIncome/?start=&end=&page=0"?>" class="list-group-item">সকল আয়ের বিবরন</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="offset1 col-lg-4" style="min-height: 250px">
                    <div class="well well-sm">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title center-align">ব্যয়</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <a  href="<?php echo base_url() ?>operator/AddExpense/" class="list-group-item">নতুন  ব্যয় </a>
                                    <a href="<?php echo base_url()."operator/ViewAllExpense/?start=&end=&page=0"?>" class="list-group-item">সকল ব্যয়ের বিবরন</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="offset2 col-lg-8" style="min-height: 250px">
                    <div class="well well-sm">
                        <div class="panel panel-info">
                            <div class="panel-heading center-align">
                                <h3 class="panel-title">খতিয়ান</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <a  href="<?php echo base_url() ?>admin/IncomeReport/" class="list-group-item"> আয়ের খতিয়ান </a>
                                    <a href="<?php echo base_url() ?>admin/ExpenseReport/" class="list-group-item"> ব্যয়ের খতিয়ান</a>
                                    <a href="#" class="list-group-item"> আয় ব্যয়ের খতিয়ান</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php include('includes/footer.php') ?>
<script type="text/javascript">

    $(document).ready(function(){
        $('.delete').click(function(){
            var result = confirm("Are you sure to delete?");
            if (result == true) {

            } else {

                return false;
            }
        });
    });

</script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo base_url().'assets/fancybox' ;?>/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo base_url().'assets/fancybox' ;?>/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/fancybox' ;?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/fancybox' ;?>/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="<?php echo base_url().'assets/fancybox' ;?>/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/fancybox' ;?>/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="<?php echo base_url().'assets/fancybox' ;?>/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="<?php echo base_url().'assets/fancybox' ;?>/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<script type="text/javascript">
    $(document).ready(function() {
        /*
         *  Simple image gallery. Uses default settings
         */

        $('.fancybox').fancybox();

        /*
         *  Different effects
         */

        // Change title type, overlay closing speed
        $(".fancybox-effects-a").fancybox({
            helpers: {
                title : {
                    type : 'outside'
                },
                overlay : {
                    speedOut : 0
                }
            }
        });

        // Disable opening and closing animations, change title type
        $(".fancybox-effects-b").fancybox({
            openEffect  : 'none',
            closeEffect	: 'none',

            helpers : {
                title : {
                    type : 'over'
                }
            }
        });

        // Set custom style, close if clicked, change title type and overlay color
        $(".fancybox-effects-c").fancybox({
            wrapCSS    : 'fancybox-custom',
            closeClick : true,

            openEffect : 'none',

            helpers : {
                title : {
                    type : 'inside'
                },
                overlay : {
                    css : {
                        'background' : 'rgba(238,238,238,0.85)'
                    }
                }
            }
        });

        // Remove padding, set opening and closing animations, close if clicked and disable overlay
        $(".fancybox-effects-d").fancybox({
            padding: 0,

            openEffect : 'elastic',
            openSpeed  : 150,

            closeEffect : 'elastic',
            closeSpeed  : 150,

            closeClick : true,

            helpers : {
                overlay : null
            }
        });

        /*
         *  Button helper. Disable animations, hide close button, change title type and content
         */

        $('.fancybox-buttons').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',

            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,

            helpers : {
                title : {
                    type : 'inside'
                },
                buttons	: {}
            },

            afterLoad : function() {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
            }
        });


        /*
         *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
         */

        $('.fancybox-thumbs').fancybox({
            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,
            arrows    : false,
            nextClick : true,

            helpers : {
                thumbs : {
                    width  : 50,
                    height : 50
                }
            }
        });

        /*
         *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
         */
        $('.fancybox-media')
            .attr('rel', 'media-gallery')
            .fancybox({
                openEffect : 'none',
                closeEffect : 'none',
                prevEffect : 'none',
                nextEffect : 'none',

                arrows : false,
                helpers : {
                    media : {},
                    buttons : {}
                }
            });

        /*
         *  Open manually
         */

        $("#fancybox-manual-a").click(function() {
            $.fancybox.open('1_b.jpg');
        });

        $("#fancybox-manual-b").click(function() {
            $.fancybox.open({
                href : 'iframe.html',
                type : 'iframe',
                padding : 5
            });
        });

        $("#fancybox-manual-c").click(function() {
            $.fancybox.open([
                {
                    href : '1_b.jpg',
                    title : 'My title'
                }, {
                    href : '2_b.jpg',
                    title : '2nd title'
                }, {
                    href : '3_b.jpg'
                }
            ], {
                helpers : {
                    thumbs : {
                        width: 75,
                        height: 50
                    }
                }
            });
        });


    });
</script>
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }
</style>
<!-- Start: MAIN CONTENT -->
<?php
/*$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$segments = explode('/', $_SERVER['REQUEST_URI_PATH']);
//print_r($segments[4]);
$page=$segments[1];
if($page=="")
    $page=0;

$loggedUserData=$this->session->all_userdata();
$loggedUser=$loggedUserData['email'];*/

if(!isset($_GET['page']))
    $page=0;
else
    $page=$_GET['page'];

?>
<div class="content">
    <div class="container">
        <div class="row">
            <?php if($message=='true') { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>
                        আয় সফল ভাবে মুছে ফেলা হয়েছে

                    </strong>
                </div>
            <?php } ?>
            <form method="get" action="<?php echo base_url()."operator/ViewAllIncome/"?>">
                <div class="form-group">
                    <div class="input-daterange" id="datepicker" >

                        <div class=" offset1 col-md-2">
                            <input  type="text" class="input-small form-control" name="start" value="<?php echo $_GET['start']  ;?>" placeholder="Start Date"  required="">
                        </div>
                        <div style="padding-top: 10px;padding-left: 25px;" class="col-md-1">থেকে</div>
                        <div class="col-md-2">
                            <input  type="text" class="input-small form-control" name="end" value="<?php echo $_GET['end']  ;?>" placeholder="End Date"  required="">
                        </div>

                    </div>
                    <div class="col-lg-1"> <input type="submit" value="Search" class="btn btn-primary"/></div>

                </div>
            </form>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="offset1 col-lg-10">
                <div class="well well-sm">
                    <table class="table table-striped" width="647">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo OF_INCOME.CRITERIA ;?></th>
                            <th><?php echo OF_INCOME.DESCRIPTION ;?></th>
                            <th><?php echo AMOUNT ;?></th>
                            <th><?php echo DATE ;?></th>
                            <th><?php echo RECEIVER_NAME ;?></th>
                            <th><?php echo VOUCHER_NO;?></th>
                            <th><?php echo VOUCHER_PICTURE;?></th>
                            <th><?php echo UPDATE ;?></th>
                            <th><?php echo DELETE ;?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($incomes as $income){?>
                        <tr>
                            <td><?php echo $income['income_id'];?></td>
                            <td>
                                 <?php echo $income['name'];?>
                            </td>

                            <td><?php echo $income['description'];?></td>
                            <td><?php echo $income['amount'];?></td>
                            <td>
                               <?php echo $income['time']?>
                            </td>
                            <td>
                                <?php echo $income['receiver_name'] ;?>
                            </td>
                            <td>
                                <?php echo $income['number']?>
                            </td>
                            <td>
                                <a class="fancybox" href="<?php echo base_url().$income['file_path'] ?>" data-fancybox-group="gallery" title="Voucher Name : <?php echo $income['voucher_name'] ?>">
                                    <img alt="<?php echo "Voucher ID: ". $income['number'] ;?>" src="<?php echo base_url().$income['file_path'] ?>" width="50" height="50"/></a>
                            </td>

                            <td>
                               <a  target="_blank" href="<?php echo base_url()?>operator/UpdateIncome/<?php echo $income['income_id']?>"> Edit </a>
                            </td>
                            <td>
                                <a class="delete"  href="<?php echo base_url()?>operator/DeleteIncome/?<?php echo 'start='.$_GET['start']."&end=".$_GET['end']."&page=".$page.'&DeleteIncomeId='.$income['income_id'] ;?>"> Delete </a>
                            </td>
                        </tr>
                    <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php  echo $this->pagination->create_links(); ?>

            </div>
        </div>
    </div>
</div>
    <!-- End: MAIN CONTENT -->


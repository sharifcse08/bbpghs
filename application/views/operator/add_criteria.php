<!-- Start: MAIN CONTENT -->
<div class="content">
    <div class="container">
        <div class="row">
            <?php if($message!='false') { ?>
                <div class="alert   <?php if(isset($success))echo 'alert-success '; else echo 'alert-danger' ;?> alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>
                        <?php
                        //if($message="validate_error")
                            echo validation_errors();
                        //else
                            echo $message;
                        ?>
                    </strong>
                </div>
            <?php } ?>

            <div class="offset2 col-lg-8">
                <div class="well well-sm">
                    <form class="form-horizontal" action="<?php echo base_url()?>operator/AddCriteria/" method="post">
                        <fieldset>
                            <legend class="text-left"><?php echo ADD_CRITERIA ;?></legend>

                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="name"><?php echo OF_CRITERIA.NAME ;?>  </label>
                                <div class="col-md-8">
                                    <input id="name" name="name" type="text" value="<?php echo set_value('name'); ?>" placeholder="Name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="type"><?php echo OF_CRITERIA.TYPE ;?></label>
                                <div class="col-lg-8">
                                    <select name="type" class="form-control">
                                        <option value="income">Income</option>
                                        <option value="expense">Expense</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="description"><?php echo OF_EXPENSE.DESCRIPTION ;?></label>
                                <div class="col-md-8">
                                    <input id="description" name="description" type="text" value="<?php echo set_value('description'); ?>" placeholder="Description" class="form-control">
                                </div>
                            </div>

                            <!-- Form actions -->
                            <div class="form-group">
                                <div class="offset1 col-md-7 text-left">
                                    <input type="hidden" name="post" value="false"/>
                                    <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End: MAIN CONTENT -->

<script>
    $(document).ready(function(){
        $('#voucher').change(function(){
            var imagePath=$(this).val();
            $('.filename').html(imagePath);
        });
    });
</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage')
                    .attr('src', e.target.result)
                    .width(350)
                    .height(300);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<!-- Start: MAIN CONTENT -->
<div class="content">
    <div class="container">
        <div class="row">
            <?php if($message!='false') { ?>
                <div class="alert   <?php if(isset($success))echo 'alert-success '; else echo 'alert-danger' ;?> alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>
                        <?php
                        //if($message="validate_error")
                            echo validation_errors();
                        //else
                            echo $message;
                        ?>
                    </strong>
                </div>
            <?php } ?>

            <div class="offset2 col-lg-8">
                <div class="well well-sm">
                    <form class="form-horizontal" action="<?php echo base_url()?>operator/UpdateExpense/<?php echo $ExpensesInfo->expense_id?>" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <legend class="text-left"><?php echo OF_EXPENSE.UPDATE ;?></legend>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo OF_EXPENSE.CRITERIA ; ?></label>
                                <div class="col-lg-7">
                                    <select name="criteria_id" class="form-control">
                                        <?php foreach($CriteriaData as $Criteria) {?>
                                            <?php if($Criteria['type']=='expense'){?>
                                                <option value="<?php echo $Criteria['id'] ?>" <?php if($Criteria['id']==$ExpensesInfo->criteria_id) echo "selected" ;?>><?php echo $Criteria['name'] ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                            <!-- Name input-->

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="description"><?php echo OF_EXPENSE.DESCRIPTION ;?></label>
                                <div class="col-md-7">
                                    <textarea id="description" name="description" placeholder="Description" class="form-control" required><?php  echo $ExpensesInfo->description ;?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="date_of_expense"><?php echo OF_EXPENSE.DATE ;?></label>
                                <div class="col-md-7">
                                    <input id="example1" name="date_of_expense" type="text" value="<?php echo $ExpensesInfo->time ;?>" placeholder="Date of Expense" class="form-control" required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="financial_year"><?php echo FINANCIAL_YEAR.DATE ;?></label>
                                <div class="col-md-7">
                                    <input id="financial_year" name="financial_year" type="text" value="<?php echo $ExpensesInfo->financial_year ;?>" placeholder="Amount" class="form-control" required="" readonly>
                                </div>
                            </div>
                            <!-- Email input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="amount"><?php echo AMOUNT ;?></label>
                                <div class="col-md-7">
                                    <input id="amount" name="amount" type="text" value="<?php echo $ExpensesInfo->amount ;?>" placeholder="Amount" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="receiver_name"><?php echo RECEIVER_NAME ;?></label>
                                <div class="col-lg-7">
                                    <select name="receiver_name" class="form-control">
                                        <?php foreach($users as $user) {?>
                                            <option value="<?php echo $user['first_name']." ".$user['last_name']?>"
                                                <?php if(($ExpensesInfo->receiver_name)==$user['first_name']." ".$user['last_name']) echo "selected" ;?>>
                                                <?php echo $user['first_name']." ".$user['last_name']?></option>
                                        <?php } ?>

                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="approver_name"><?php echo APPROVER_NAME ;?></label>
                                <div class="col-lg-7">
                                    <select name="approver_name" class="form-control">
                                        <?php foreach($users as $user) {?>
                                            <option value="<?php echo $user['first_name']." ".$user['last_name']?>"
                                                <?php if(($ExpensesInfo->approver_name)==$user['first_name']." ".$user['last_name']) echo "selected" ;?>>
                                                <?php echo $user['first_name']." ".$user['last_name']?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="file_number"><?php echo VOUCHER_NO;?></label>
                                <div class="col-md-7">
                                    <input id="voucher_id" name="voucher_id" type="text" value="<?php echo $ExpensesInfo->number ;?>" placeholder="Voucher ID" class="form-control" autocomplete="off" required >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="voucher"><?php echo VOUCHER_PICTURE ;?></label>
                                <div class="col-md-7">
                                    <input id="voucher" name="voucher" type="file" onchange="readURL(this);" class="form-control" autocomplete="off"  >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="voucher"></label>
                                <div class="col-md-7">
                                    <img id="showImage" width="250" height="200" src="<?php echo base_url().$ExpensesInfo->file_path ; ;?>" />
                                    <br> <br>
                                    <span style="color: green;" class="filename"></span>

                                </div>
                            </div>
                            <!-- Form actions -->
                            <div class="form-group">
                                <div class="offset2 col-md-7 text-left">
                                    <input type="hidden" name="post" value="false"/>
                                    <button type="submit" class="btn btn-primary btn-lg"><?php echo SUBMIT ;?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End: MAIN CONTENT -->

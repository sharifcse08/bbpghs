<script>
    $(document).ready(function(){
        $('#voucher').change(function(){
            var imagePath=$(this).val();
            $('.filename').html(imagePath);
        });
    });
</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage')
                    .attr('src', e.target.result)
                    .width(350)
                    .height(300);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<!-- Start: MAIN CONTENT -->
<div class="content">
    <div class="container">
        <div class="row">
            <?php if($message!='false') { ?>
                <div class="alert   <?php if(isset($success))echo 'alert-success '; else echo 'alert-danger' ;?> alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>
                        <?php
                        //if($message="validate_error")
                            echo validation_errors();
                        //else
                            echo $message;
                        ?>
                    </strong>
                </div>
            <?php } ?>
            <div class="offset2 col-lg-8">
                <div class="well well-sm">
                    <form class="form-horizontal" action="<?php echo base_url()?>operator/AddIncome/" method="post" enctype="multipart/form-data">
                    <fieldset>
                            <legend class="text-left">আয় সংযুক্ত করুন</legend>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo OF_INCOME.CRITERIA ;?></label>
                                <div class="col-lg-7">
                                    <select name="criteria_id" class="form-control">
                                        <?php foreach($CriteriaData as $Criteria) {?>
                                            <?php if($Criteria['type']=='income'){?>
                                                <option value="<?php echo $Criteria['id'] ?>"><?php echo $Criteria['name'] ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="description"><?php echo OF_INCOME.DESCRIPTION ;?></label>
                                <div class="col-md-7">
                                    <textarea id="description" name="description"placeholder="Description" class="form-control" required><?php echo set_value('description'); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="date_of_income"><?php echo OF_INCOME.DATE ;?></label>
                                <div class="col-md-7">
                                    <input id="example1" name="date_of_income" type="text" value="<?php echo date('Y-m-d'); ?>" placeholder="Date of Income" class="form-control" required readonly>
                              </div>
                            </div>
                            <?php
                            $fyear=DATE('Y');

                            if(date('m')>6)
                                $fyear=$fyear+1;
                            else
                                $fyear=$fyear;

                            ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="financial_year"><?php echo FINANCIAL_YEAR ;?></label>
                                <div class="col-md-7">
                                    <input id="financial_year" name="financial_year" type="text" value="<?php echo ($fyear-1)."-".($fyear) ;?>" placeholder="Amount" class="form-control" required="" readonly>
                                </div>
                            </div>
                            <!-- Email input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="amount"><?php echo AMOUNT ;?></label>
                                <div class="col-md-7">
                                    <input id="amount" name="amount" type="text" value="<?php echo set_value('amount'); ?>" placeholder="Amount" class="form-control" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="receiver_name"><?php echo RECEIVER_NAME ;?></label>
                                <div class="col-lg-7">
                                    <select name="receiver_name" class="form-control">
                                        <?php foreach($users as $user) {?>
                                            <option value="<?php echo $user['first_name']." ".$user['last_name']?>"><?php echo $user['first_name']." ".$user['last_name']?></option>
                                        <?php } ?>

                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="voucher_id"><?php echo VOUCHER_NO ;?></label>
                                <div class="col-md-7">
                                    <input id="voucher_id" name="voucher_id" type="text" value="<?php echo set_value('voucher_id'); ?>" placeholder="Voucher Id" class="form-control" autocomplete="off" required >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="voucher"><?php echo VOUCHER_PICTURE ;?></label>
                                <div class="col-md-7">
                                    <input id="voucher" name="voucher" type="file"  onchange="readURL(this);" class="form-control" autocomplete="off" required >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="voucher"></label>
                                <div class="col-md-7">
                                    <img id="showImage" width="250" height="200" src="#" alt="রসিদ নির্বাচন করুন" />
                                    <br> <br>
                                    <span style="color: green;" class="filename"></span>

                                </div>
                            </div>
                            <!-- Form actions -->
                            <div class="form-group">
                                <div class="offset2 col-md-7 text-left">
                                    <input type="hidden" name="post" value="false"/>
                                    <button type="submit" class="btn btn-primary btn-lg"><?php echo SUBMIT ;?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End: MAIN CONTENT -->

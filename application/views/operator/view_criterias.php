<script type="text/javascript">

    $(document).ready(function(){
        $('.delete').click(function(){
            var result = confirm("Are you sure to delete?");
            if (result == true) {

            } else {

                return false;
            }
        });
    });

</script>

<!-- Start: MAIN CONTENT -->
<?php
$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$segments = explode('/', $_SERVER['REQUEST_URI_PATH']);
//print_r($segments[4]);
$page=$segments[4];
if($page=="")
    $page=0;

$loggedUserData=$this->session->all_userdata();
$loggedUser=$loggedUserData['email'];


?>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="offset1 col-lg-10">
                <div class="well well-sm">
                    <table class="table table-striped" width="647">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo CRITERIA;?></th>
                            <th><?php echo TYPE;?></th>
                            <th><?php echo DESCRIPTION;?></th>
                            <th><?php echo UPDATE;?></th>
                            <th><?php echo DELETE;?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($Criterias as $criteria){?>
                        <tr>
                            <td><?php echo $criteria['id'];?></td>
                            <td>
                                 <?php echo $criteria['name'];?>
                            </td>
                            <td>
                                <?php echo $criteria['type'];?>
                            </td>
                            <td><?php echo $criteria['description'];?></td>
                            <td>
                               <a  target="_blank" href="<?php echo base_url()?>operator/UpdateCriteria/<?php echo $criteria['id']?>"> Edit </a>
                            </td>
                            <td>
                                <a class="delete"  href="<?php echo base_url()?>operator/DeleteCriteria/<?php echo $page.'/'.$criteria['id'] ;?>"> Delete </a>
                            </td>
                        </tr>
                    <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php  echo $this->pagination->create_links(); ?>

            </div>
        </div>
    </div>
</div>
    <!-- End: MAIN CONTENT -->

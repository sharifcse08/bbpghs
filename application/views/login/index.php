 <!-- Start: MAIN CONTENT -->
 <div class="content">
     <div class="container">
         <!--div class="page-header">
             <h1><?php //echo WELCOMETEXT ;?></h1>
         </div-->
         <?php if(isset($error)) { ?>
             <div class="alert alert-danger alert-dismissable">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                 <strong>
                     <?php
                         if(isset($error_message))
                             echo $error_message;
                         echo validation_errors();
                     ?>
                 </strong>
             </div>
         <?php } ?>
         <div class="row">
             <div class="offset3 col-lg-6">
                 <h4 class="widget-header"><i class="icon-lock"></i> <?php echo WELCOMETEXT ;?></h4>
                 <div class="widget-body">
                     <div class="center-align">
                         <div class="well well-sm">
                             <form method="post"  action="<?php echo base_url()?>login/Check/" class="form-horizontal form-signin-signup" autocomplete="off">

                                 <fieldset>

                                     <!-- Name input-->
                                     <div class="form-group">
                                         <label class="col-md-4 control-label" for="name"><?php echo USERNAME ;?> </label>
                                         <div class="col-md-8">
                                             <input id="username" name="username" type="text" placeholder="Email / mobile number" class="form-control" autocomplete="off">
                                         </div>
                                     </div>

                                     <!-- Email input-->
                                     <div class="form-group">
                                         <label class="col-md-4 control-label" for="email"><?php echo PASSWORD ;?> </label>
                                         <div class="col-md-8">
                                             <input id="password" name="password" type="password" placeholder="Password" class="form-control">
                                         </div>
                                     </div>
                                     <div class="form-group">
                                         <!--div class="col-md-4 offset1">
                                             <input type="checkbox"> Remember me
                                         </div-->

                                         <!--div class="col-md-4">
                                             <a href="<?php echo base_url() ;?>login/ForgotPassword/">Forgot password?</a>
                                         </div-->
                                         <div class="clearfix"></div>
                                     </div>
                                     <input type="submit" value="<?php echo SIGNIN2 ;?>" class="btn btn-primary btn-lg">
                                     </fieldset>
                                 </form>
                             </div>

                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!-- End: MAIN CONTENT -->

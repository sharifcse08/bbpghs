<script type="text/javascript">
    $(document).ready(function(){
        $('#form').submit(function(){

            var regEx = /^\d{11}$/;

            var val = $("#phone").val();
            if (!val.match(regEx)) {
                alert('Invalid Phone Number');
                return false;
            }
        });

    });
</script>


<!-- Start: MAIN CONTENT -->
<div class="content">
    <div class="container">
        <div class="row">
            <?php if($message) { ?>
                <div class="alert   <?php if(isset($success))echo 'alert-success '; else echo 'alert-danger' ;?> alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>
                        <?php
                        if($message=="validate_error")
                            echo validation_errors();
                        else
                            echo $message;

                        ?>
                    </strong>
                </div>
            <?php } ?>

            <div class="offset2 col-lg-8">
                <div class="well well-sm">
                    <form id="form" class="form-horizontal" action="<?php echo base_url()?>admin/EditUser/<?php echo $userInfo->user_id ;?>" method="post">
                        <fieldset>
                            <legend class="text-left"><?php echo UPDATE_USER ; ?></legend>

                            <!-- Name input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo FIRST_NAME ; ?></label>
                                <div class="col-md-7">
                                    <input id="fname" name="fname" type="text" value="<?php echo $userInfo->first_name ;?>" placeholder="First Name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo LAST_NAME ; ?></label>
                                <div class="col-md-7">
                                    <input id="lname" name="lname" type="text" value="<?php echo $userInfo->last_name ;?>" placeholder="Last Name" class="form-control">
                                </div>
                            </div>
                            <!-- Email input-->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="email"><?php echo EMAIL ; ?></label>
                                <div class="col-md-7">
                                    <input id="email" name="email" type="text" value="<?php echo $userInfo->email ;?>" placeholder="Email" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo PASSWORD ; ?></label>
                                <div class="col-md-7">
                                    <input id="password" name="password" type="password" value="<?php echo $userInfo->password ;?>" placeholder="Password" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo CONFIRM_PASSWORD ; ?></label>
                                <div class="col-md-7">
                                    <input id="cpassword" name="cpassword" type="password" value="<?php echo $userInfo->password ;?>" placeholder="Confirm Password" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo ROLE ; ?></label>
                                <div class="col-lg-7">
                                    <select name="role" class="form-control">
                                        <?php foreach($roles as $role) {?>
                                             <option value="<?php echo $role['role_id'] ?>" <?php if($role['role_id']==$userInfo->role) echo "selected";?> ><?php echo $role['role_name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo DESIGNATION ; ?></label>
                                <div class="col-lg-7">
                                    <select name="designation" class="form-control">

                                        <?php
                                        $designations=explode(',',DESIGNATIONS );
                                        ?>
                                        <?php foreach ($designations as $designation) {?>
                                            <option value="<?php echo $designation ;?>" <?php if($designation==$userInfo->designation) echo "selected" ;?>><?php echo $designation ;?></option>

                                        <?php } ?>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="gender"><?php echo GENDER ; ?></label>
                                <div class=" col-lg-7">
                                    <label >
                                        <input type="radio" name="gender" id="male" value="male" <?php if($userInfo->gender=="male") echo "checked" ?>>
                                        Male
                                    </label>
                                    &nbsp;&nbsp;&nbsp;
                                    <label >
                                        <input type="radio" name="gender" id="female" value="female" <?php if($userInfo->gender=="female") echo "checked" ?>>
                                        Female
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="name"><?php echo MOBILE ; ?></label>
                                <div class="col-md-7">
                                    <input id="phone" name="phone" type="text" value="<?php echo $userInfo->contact_number ;?>" placeholder="phone" class="form-control">
                                </div>
                            </div>
                            <!-- Message body -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="message"><?php echo ADDRESS ; ?></label>
                                <div class="col-md-7">
                                    <textarea class="form-control" id="address" name="address"  placeholder="Address" rows="5"><?php echo $userInfo->address ;?></textarea>
                                </div>
                            </div>

                            <input type="hidden"  name="user_id" value="<?php echo $userInfo->user_id; ?>"/>
                            <!-- Form actions -->
                            <div class="form-group">
                                <div class="offset2 col-md-7 text-left">
                                    <button type="submit" class="btn btn-primary btn-lg"><?php echo UPDATE ; ?></button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- End: MAIN CONTENT -->

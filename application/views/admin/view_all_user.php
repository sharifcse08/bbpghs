<!-- Start: MAIN CONTENT -->
<?php
$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$segments = explode('/', $_SERVER['REQUEST_URI_PATH']);
//print_r($segments[4]);
$page=$segments[4];
if($page=="")
    $page=0;

$loggedUserData=$this->session->all_userdata();
$loggedUser=$loggedUserData['email'];

?>
<div class="content">
    <div class="container">
        <div class="row">

            <div class="offset1 col-lg-10">
                <div class="well well-sm">
                    <table class="table table-striped" width="647">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo NAME ; ?></th>
                            <th><?php echo EMAIL ; ?></th>
                            <th><?php echo GENDER ; ?></th>
                            <th><?php echo MOBILE ; ?></th>
                            <th><?php echo ROLE ; ?></th>
                            <th><?php echo DESIGNATION ; ?></th>
                            <th><?php echo USER_UPDATE ; ?></th>
                            <th><?php echo BLOCK."/".UNBLOCK ; ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  foreach($users as $user){?>
                        <tr>
                            <td><?php echo $user['user_id'];?></td>
                            <td>
                                 <?php echo $user['first_name']." ".$user['last_name'];?>
                            </td>
                            <td>
                                <?php echo $user['email']?>
                            </td>
                            <td>
                                <?php echo $user['gender']?>
                            </td>
                            <td>
                                <?php echo $user['contact_number']?>
                            </td>
                            <td><?php echo $user['role_name'];?></td>
                            <td>
                                <?php echo $user['designation']?>
                            </td>
                            <td>
                                <?php if($user['role_id']!=1) {?>
                                    <a href="<?php echo base_url()?>admin/Edit/<?php echo $user['user_id']?>"> Edit </a>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if($user['role_id']!=1) {?>
                                    <?php if($user['is_active']==1){?>
                                        <a href="<?php echo base_url()?>admin/MakeInactiveUser/<?php echo $page."/".$user['user_id']?>"> Block</a>
                                    <?php }
                                    else {?>
                                        <a href="<?php echo base_url()?>admin/MakeActiveUser/<?php echo $page."/".$user['user_id']?>"> Unlock</a>
                                    <?php } ?>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php  echo $this->pagination->create_links(); ?>

            </div>
        </div>
    </div>
</div>
    <!-- End: MAIN CONTENT -->

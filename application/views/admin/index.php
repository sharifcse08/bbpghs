<script type="text/javascript">
    $(document).ready(function(){

        $('.panel-info .list-group-item').hover(function(){
            $(this).css('background-color','#428BCA');
            $(this).css('color','#FFFFFF')
        }, function(){
            $(this).css('background-color','#FFFFFF');
            $(this).css('color','#333333');
        });

        $('.panel-danger .list-group-item').hover(function(){
            $(this).css('background-color','#EED3D7');
            $(this).css('color','#000000')
        }, function(){
            $(this).css('background-color','#FFFFFF');
            $(this).css('color','#333333');
        });
    });

</script>

<?php
$session_data=$this->session->all_userdata();
$user_role=$session_data['role_id'];
?>
<!-- Start: MAIN CONTENT -->
<div class="content">
    <div class="container">
        <div class="row">

            <div class="offset1 col-lg-4" style="min-height: 250px">
                <div class="well well-sm">
                    <div class="panel <?php if($user_role!=1) echo "panel-danger"; else echo "panel-info"?>">
                        <div class="panel-heading ">
                            <h3 class="panel-title  center-align">ব্যবহারকারী</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group ">
                                <a href="<?php if($user_role==1) echo base_url()."admin/Create/";else echo"#"; ?>" class="list-group-item" >নতুন ব্যবহারকারী </a>
                                <a href="<?php if($user_role==1) echo base_url()."admin/ViewAllUser/";else echo"#";?>"class="list-group-item">সকল ব্যবহারকারীর তথ্য </a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offset1 col-lg-4" style="min-height: 250px">
                <div class="well well-sm">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title center-align">খাত সমূহ</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <a  href="<?php echo base_url() ?>operator/AddCriteria/" class="list-group-item">নতুন খাত </a>
                                <a href="<?php echo base_url()?>operator/ViewAllCriteria/" class="list-group-item">সকল খাতের বিবরণ </a>
                            </ul>
                        </div>
                    </div>
                </div>

        </div>

            <div class="offset1 col-lg-4" style="min-height: 250px">
                <div class="well well-sm">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title center-align">আয়</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <a  href="<?php echo base_url() ?>operator/AddIncome/" class="list-group-item">নতুন  আয় </a>
                                <a href="<?php echo base_url()."operator/ViewAllIncome/?start=&end=&page=0"?>" class="list-group-item">সকল আয়ের বিবরন</a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div class="offset1 col-lg-4" style="min-height: 250px">
                <div class="well well-sm">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title center-align">ব্যয়</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <a  href="<?php echo base_url() ?>operator/AddExpense/" class="list-group-item">নতুন  ব্যয় </a>
                                <a href="<?php echo base_url()."operator/ViewAllExpense/?start=&end=&page=0"?>" class="list-group-item">সকল ব্যয়ের বিবরন</a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="offset1 col-lg-4" style="min-height: 250px">
                <div class="well well-sm">
                    <div class="panel panel-info">
                        <div class="panel-heading center-align">
                            <h3 class="panel-title">প্রতিবেদন</h3>
                        </div>
                        <?php
                        $fyear=DATE('Y');

                        if(date('m')>6)
                            $fyear=$fyear+1;
                        else
                            $fyear=$fyear;

                        $start=($fyear-1)."-07-01";
                        $end= $fyear."-06-30";
                        ?>
                        <div class="panel-body">
                            <ul class="list-group">
                                <a  href="<?php echo base_url() ?>operator/IncomeReport/" class="list-group-item"> আয়ের বার্ষিক প্রতিবেদন </a>
                                <a href="<?php echo base_url() ?>operator/ExpenseReport/" class="list-group-item"> ব্যয়ের বার্ষিক  প্রতিবেদন</a>
                                <a href="<?php echo base_url() ?>operator/Report/?start=<?php echo $start ;?>&end=<?php echo $end ;?>&type=income&criteria_id=0" class="list-group-item"> আয় - ব্যয়</a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offset1 col-lg-4" style="min-height: 250px">
                <div class="well well-sm">
                    <div class="panel panel-info">
                        <div class="panel-heading center-align">
                            <h3 class="panel-title">ডাটা ব্যাকআপ</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <a  href="<?php echo base_url() ?>operator/CreateBackup/" class="list-group-item ">
                                    <button class="btn btn-danger " style="width: 100%;">সংরক্ষণ করুন</button>
                                </a>
                                <a  href="<?php echo base_url() ?>operator/UploadBackup/" class="list-group-item ">
                                    <button class="btn btn-danger " style="width: 100%;">রিস্টোর করুন</button>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End: MAIN CONTENT -->

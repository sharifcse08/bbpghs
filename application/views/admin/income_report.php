<!-- Start: MAIN CONTENT -->
<div class="container">
    <div class="row">
        <form method="post" action="<?php echo base_url(); ?>operator/IncomeReport">
            <div class="form-group">
                <div class="col-lg-2">
                    <?php echo FINANCIAL_YEAR ?>:
                </div>
                <div class="col-lg-2">
                    <select name="financial_year" class="form-control col-lg-2">
                        <?php
                        $fyear=DATE('Y');

                        if(date('m')>6)
                            $fyear=$fyear+1;
                        else
                            $fyear=$fyear;

                        ?>
                        <?php for($i=$fyear;$i>2010;$i--){?>
                            <option value="<?php echo $i ;?>" <?php  if($i==$year) echo "selected"; ?>><?php echo ($i-1)."-".($i) ;?></option>
                        <?php } ?>

                    </select>
                </div>

                <div class=" col-lg-2">
                    <input class="form-control btn btn-default btn-primary" value="<?php echo OF_INCOME.CREATE_REPORT ;?>" type="submit"/>
                </div>

            </div>
        </form>
        <div class="col-lg-2">
            <a  target="_blank" href="<?php echo base_url();?>operator/getPDF/income/<?php echo $year;?>" class="form-control btn btn-default btn-primary btn-danger">সংরক্ষণ</a>
        </div>
    </div>
</div>

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="well well-sm">
                    <table class="table table-bordered" width="647">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>JULY</th>
                            <th>AUGUST</th>
                            <th>SEPTEMBER</th>
                            <th>OCTOBER</th>
                            <th>NOVEMBER</th>
                            <th>DECEMBER</th>
                            <th>JANUARY</th>
                            <th>FEBRUARY</th>
                            <th>MARCH</th>
                            <th>APRIL</th>
                            <th>MAY</th>
                            <th>JUNE</th>
                            <th>TOTAL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $TotalSum=0; ?>
                        <?php  foreach($MonthData as $Data){?>
                        <tr>
                            <?php foreach($Data as $month){?>
                                <td><?php echo $month ?></td>
                            <?php } ?>
                            <td>
                                <?php
                                $sum=0;
                                for($i=1;$i<=12;$i++)
                                {
                                    $sum+=$Data[$i];
                                }
                                $TotalSum+=$sum;
                                echo $sum;

                                ?></td>
                        </tr>
                        <?php } ?>
                        <tr ><td colspan="13" style="text-align: center;"> <?php echo FINANCIAL_YEAR .":"?> <?php echo  ($year-1)."--".($year) ;?>  </td> <td><?php echo $TotalSum ;?></td></tr>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
</div>
    <!-- End: MAIN CONTENT -->

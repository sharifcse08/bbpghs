<?php
class Admindb extends CI_Model {

    public function __construct()
    {
        //echo "varsity";
    }


    public function saveUser()
    {
        $userdata=array(
            'first_name'=>$this->input->post('fname'),
            'last_name'=>$this->input->post('lname'),
            'password'=>$this->input->post('password'),
            'email'=>$this->input->post('email'),
            'gender'=>$this->input->post('gender'),
            'contact_number'=>$this->input->post('phone'),
            'role'=>$this->input->post('role'),
            'designation'=>$this->input->post('designation'),
            'address'=>$this->input->post('address')
        );
        $this->db->insert('users',$userdata);

        $this->db->select('*');
        $this->db->from('users');
        $this->db->limit(1);
        $this->db->order_by('user_id','desc');
        $query=$this->db->get();
        return $query->row();
    }
    public function saveChangedInfo($user_id)
    {
        $userdata=array(
            'first_name'=>$this->input->post('fname'),
            'last_name'=>$this->input->post('lname'),
            'password'=>$this->input->post('password'),
            'gender'=>$this->input->post('gender'),
            'contact_number'=>$this->input->post('phone'),
            'role'=>$this->input->post('role'),
            'designation'=>$this->input->post('designation'),
            'address'=>$this->input->post('address')
        );
        $this->db->where('user_id',$user_id);
        $this->db->update('users',$userdata);
    }
    public function viewUser($user_id=0)
    {
        $this->db->select('*');
        $this->db->from('users');
        if($user_id!=0)
            $this->db->where('user_id',$user_id);
        $query=$this->db->get();
        if($user_id!=0)
            return $query->row();
        else
            return $query->result_array();
    }
    public function viewAllUser($index, $limit)
    {
        $this->db->select('user_id,designation,contact_number,gender,users.role,users.email, users.is_active, users.first_name, users.last_name, user_role.role_id,user_role.role_name');
        $this->db->from('users');
        $this->db->join('user_role','users.role=user_role.role_id');
        $this->db->limit($limit,$index);
        $this->db->order_by('first_name','asc');
        $this->db->order_by('last_name','asc');
        $query=$this->db->get();
        return $query->result_array();
    }
    public function MakeInactiveUser($UserId)
    {
        $this->db->where('user_id',$UserId);
        $this->db->update('users',array('is_active'=>0));
    }
    public function MakeActiveUser($UserId)
    {
        $this->db->where('user_id',$UserId);
        $this->db->update('users',array('is_active'=>1));
    }

    public function getRoles()
    {
        $this->db->select('*');
        $this->db->from('user_role');
        $query=$this->db->get();
        return $query->result_array();
    }
    public function addCriteria()
     {
         $data=array(
             'name'=>$this->input->post('name'),
             'type'=>$this->input->post('type'),
             'description'=>$this->input->post('description')
         );
         $this->db->insert('criteria',$data);

         $this->db->select('*');
         $this->db->from('criteria');
         $this->db->limit(1);
         $this->db->order_by('id','desc');
         $query=$this->db->get();
         return $query->row();

     }
    public function getCriteria($criteria_id=0)
    {
        $this->db->select('*');
        $this->db->from('criteria');
        if($criteria_id!=0)
            $this->db->where('id',$criteria_id);
        $query=$this->db->get();
        if($criteria_id!=0)
            return $query->row();
        else
            return $query->result_array();
    }
    public function saveChangedCriteriaInfo($CriteriaID)
    {
        $data=array(
            'name'=>$this->input->post('name'),
            'type'=>$this->input->post('type'),
            'description'=>$this->input->post('description')
        );

        $this->db->where('id',$CriteriaID);
        $this->db->update('criteria',$data);
    }
    public function addExpensesInfo($VoucherData)
    {

        $this->db->insert('file',$VoucherData);
        $this->db->select('*');
        $this->db->from('file');
        $this->db->limit(1);
        $this->db->order_by('id','desc');
        $query=$this->db->get();

        $File=$query->row();

        $SessionData=$this->session->all_userdata();

        $data=array(
            'criteria_id'=>$this->input->post('criteria_id'),
            'description'=>$this->input->post('description'),
            'time'=>$this->input->post('date_of_income'),
            'financial_year'=>$this->input->post('financial_year'),
            'amount'=>$this->input->post('amount'),
            'receiver_name'=>$this->input->post('receiver_name'),
            'approver_name'=>$this->input->post('approver_name'),
            'voucher_id'=>$File->id,
            'modified_id'=>$SessionData['userid']
        );
        $this->db->insert('expense',$data);

        $this->db->select('*');
        $this->db->from('expense');
        $this->db->limit(1);
        $this->db->order_by('id','desc');
        $query=$this->db->get();
        $ExpensesData=$query->row();
        return $ExpensesData->id;
    }
    public function incomeReport($year1,$year2)
    {

        $YearRange1=$year1."-07-01";
        $YearRange2=$year2."-06-30";

        $report=array();

        $this->db->select('*');
        $this->db->from('criteria');
        $this->db->where("type","income");

        $query=$this->db->get();

        $criterias= $query->result_array();


        foreach($criterias as $criteria)
        {
            //echo($criteria['name']);

            $this->db->select("SUM(amount) AS MySum, monthname(time) AS MonthName");
            $this->db->from("income");
            $this->db->join('criteria','criteria.id=income.criteria_id');
            $this->db->join('file','income.voucher_id=file.file_id');
            $this->db->where("criteria_id", $criteria['id']);
            $this->db->where('time >=',$YearRange1);
            $this->db->where('time <=', $YearRange2);
            $this->db->group_by('MONTH(time)');
            $this->db->order_by('YEAR(time)',"asc");
            $query=$this->db->get();

            //echo json_encode($query->result_array())."<br>";;
            $criteria_data=$query->result_array();
            $month=array();

            $month[0]=$criteria['name'];
            $month[1]=NULL;
            $month[2]=NULL;
            $month[3]=NULL;
            $month[4]=NULL;
            $month[5]=NULL;
            $month[6]=NULL;
            $month[7]=NULL;
            $month[8]=NULL;
            $month[9]=NULL;
            $month[10]=NULL;
            $month[11]=NULL;
            $month[12]=NULL;
            foreach($criteria_data as $criteria)
            {
                if($criteria['MonthName']=="January")
                    $month[7]=$criteria['MySum'];
                if($criteria['MonthName']=="February")
                    $month[8]=$criteria['MySum'];
                if($criteria['MonthName']=="March")
                    $month[9]=$criteria['MySum'];
                if($criteria['MonthName']=="April")
                    $month[10]=$criteria['MySum'];
                if($criteria['MonthName']=="May")
                    $month[11]=$criteria['MySum'];
                if($criteria['MonthName']=="June")
                    $month[12]=$criteria['MySum'];
                if($criteria['MonthName']=="July")
                    $month[1]=$criteria['MySum'];
                if($criteria['MonthName']=="August")
                    $month[2]=$criteria['MySum'];
                if($criteria['MonthName']=="September")
                    $month[3]=$criteria['MySum'];
                if($criteria['MonthName']=="October")
                    $month[4]=$criteria['MySum'];
                if($criteria['MonthName']=="November")
                    $month[5]=$criteria['MySum'];
                if($criteria['MonthName']=="December")
                    $month[6]=$criteria['MySum'];
            }

            array_push($report,$month);
        }

        //echo json_encode($report);
        return $report;
    }
    public function expenseReport($year1,$year2)
    {

        $YearRange1=$year1."-07-01";
        $YearRange2=$year2."-06-30";

        $report=array();

        $this->db->select('*');
        $this->db->from('criteria');
        $this->db->where("type","expense");

        $query=$this->db->get();

        $criterias= $query->result_array();

        foreach($criterias as $criteria)
        {
            //echo($criteria['name']);

            $this->db->select("SUM(amount) AS MySum, monthname(time) AS MonthName");
            $this->db->from("expense");
            $this->db->join('criteria','criteria.id=expense.criteria_id');
            $this->db->join('file','expense.voucher_id=file.file_id');
            $this->db->where("criteria_id", $criteria['id']);
            $this->db->where('time >=',$YearRange1);
            $this->db->where('time <=', $YearRange2);
            $this->db->group_by('MONTH(time)');
            $this->db->order_by('YEAR(time)',"asc");
            $query=$this->db->get();

            //echo json_encode($query->result_array())."<br>";;
             $criteria_data=$query->result_array();
            $month=array();

            $month[0]=$criteria['name'];
            $month[1]=NULL;
            $month[2]=NULL;
            $month[3]=NULL;
            $month[4]=NULL;
            $month[5]=NULL;
            $month[6]=NULL;
            $month[7]=NULL;
            $month[8]=NULL;
            $month[9]=NULL;
            $month[10]=NULL;
            $month[11]=NULL;
            $month[12]=NULL;
             foreach($criteria_data as $criteria)
             {
                 if($criteria['MonthName']=="January")
                     $month[7]=$criteria['MySum'];
                 if($criteria['MonthName']=="February")
                     $month[8]=$criteria['MySum'];
                 if($criteria['MonthName']=="March")
                     $month[9]=$criteria['MySum'];
                 if($criteria['MonthName']=="April")
                     $month[10]=$criteria['MySum'];
                 if($criteria['MonthName']=="May")
                     $month[11]=$criteria['MySum'];
                 if($criteria['MonthName']=="June")
                     $month[12]=$criteria['MySum'];
                 if($criteria['MonthName']=="July")
                     $month[1]=$criteria['MySum'];
                 if($criteria['MonthName']=="August")
                     $month[2]=$criteria['MySum'];
                 if($criteria['MonthName']=="September")
                     $month[3]=$criteria['MySum'];
                 if($criteria['MonthName']=="October")
                     $month[4]=$criteria['MySum'];
                 if($criteria['MonthName']=="November")
                     $month[5]=$criteria['MySum'];
                 if($criteria['MonthName']=="December")
                     $month[6]=$criteria['MySum'];
             }

            array_push($report,$month);
        }

        //echo json_encode($report);
        return $report;
    }
    public function validate_phone_number($number,$user_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('contact_number',$number);
        $this->db->where('user_id !=',$user_id);
        $query=$this->db->get();
        return $query->result_array();
    }


}
?>
<?php
class Operatordb extends CI_Model {

    public function __construct()
    {
        //echo "varsity";
    }
    public function getRoles()
    {
        $this->db->select('*');
        $this->db->from('user_role');
        $query=$this->db->get();
        return $query->result_array();
    }
    public function addCriteria()
     {
         $data=array(
             'name'=>$this->input->post('name'),
             'type'=>$this->input->post('type'),
             'description'=>$this->input->post('description')
         );
         $this->db->insert('criteria',$data);

         $this->db->select('*');
         $this->db->from('criteria');
         $this->db->limit(1);
         $this->db->order_by('id','desc');
         $query=$this->db->get();
         return $query->row();

     }
    public function getCriteria($criteria_id=0)
    {
        $this->db->select('*');
        $this->db->from('criteria');
        if($criteria_id!=0)
            $this->db->where('id',$criteria_id);
        $query=$this->db->get();
        if($criteria_id!=0)
            return $query->row();
        else
            return $query->result_array();
    }
    public function getCriteriaByType($type)
    {
        $this->db->select('*');
        $this->db->from('criteria');
        $this->db->where('type',$type);
        $query=$this->db->get();
        return $query->result_array();
    }
    public function saveChangedCriteriaInfo($CriteriaID)
    {
        $data=array(
            'name'=>$this->input->post('name'),
            'type'=>$this->input->post('type'),
            'description'=>$this->input->post('description')
        );

        $this->db->where('id',$CriteriaID);
        $this->db->update('criteria',$data);
    }
    public function viewCriteria($index=0, $limit=0)
    {
        $this->db->select('*');
        $this->db->from('criteria');

        if($limit!=0)
            $this->db->limit($limit,$index);
        $query=$this->db->get();
        return $query->result_array();
    }
    public function deleteCriteriaId($CriteriaId)
    {
        $this->db->where('criteria_id',$CriteriaId);
        $this->db->delete('income');

        $this->db->where('criteria_id',$CriteriaId);
        $this->db->delete('expense');


        $this->db->where('id',$CriteriaId);
        $this->db->delete('criteria');
    }
    public function addIncomeInfo($VoucherData)
    {

        $this->db->insert('file',$VoucherData);

        $this->db->select('*');
        $this->db->from('file');
        $this->db->limit(1);
        $this->db->order_by('file_id','desc');
        $query=$this->db->get();

        $File=$query->row();

        $SessionData=$this->session->all_userdata();

        $data=array(
            'criteria_id'=>$this->input->post('criteria_id'),
            'description'=>$this->input->post('description'),
            'time'=>$this->input->post('date_of_income'),
            'financial_year'=>$this->input->post('financial_year'),
            'amount'=>$this->input->post('amount'),
            'receiver_name'=>$this->input->post('receiver_name'),
            'voucher_id'=>$File->file_id,
            'modified_id'=>$SessionData['userid']
        );
        $this->db->insert('income',$data);

        $this->db->select('*');
        $this->db->from('income');
        $this->db->limit(1);
        $this->db->order_by('income_id','desc');
        $query=$this->db->get();
        $ExpensesData=$query->row();
        return $ExpensesData->income_id;
    }
    public function getIncome($IncomeId=0)
    {
        $this->db->select('*');
        $this->db->from('income');
        $this->db->join('file','income.voucher_id=file.file_id');
        if($IncomeId!=0)
            $this->db->where(array('income.income_id'=>$IncomeId));
        $this->db->order_by('income_id','desc');
        $query=$this->db->get();
        if($IncomeId!=0)
            return $query->row();
        else
            return $query->result_array();
    }
    public function updateIncomeInfo($IncomeId,$VoucherData)
    {
        if($VoucherData!=NULL)
        {
            $this->db->select('*');
            $this->db->from('income');
            $this->db->limit(1);
            $this->db->where('income_id',$IncomeId);
            $query=$this->db->get();
            $deleteData=$query->row();

            $this->db->where('file_id',$deleteData->voucher_id);
            $this->db->update('file',$VoucherData);
        }

        $SessionData=$this->session->all_userdata();

        $data=array(
            'criteria_id'=>$this->input->post('criteria_id'),
            'description'=>$this->input->post('description'),
            'time'=>$this->input->post('date_of_income'),
            'financial_year'=>$this->input->post('financial_year'),
            'amount'=>$this->input->post('amount'),
            'receiver_name'=>$this->input->post('receiver_name'),
            'modified_id'=>$SessionData['userid']
        );

        $this->db->where('income_id',$IncomeId);
        $this->db->update('income',$data);

    }
    public function deleteIncomeId($IncomeId)
    {

        $this->db->select('*');
        $this->db->from('income');
        $this->db->limit(1);
        $this->db->where('income_id',$IncomeId);
        $query=$this->db->get();
        $deleteData=$query->row();

        $this->db->where('file_id',$deleteData->voucher_id);
        $this->db->delete('file');

        $this->db->where('income_id',$IncomeId);
        $this->db->delete('income');
    }
    public function viewAllIncome($start, $end, $index=0, $limit=0 )
    {
        $this->db->select('income.income_id,criteria.id,criteria.name, income.description,income.amount,file_path,number,voucher_name, income.receiver_name, income.time');

        $this->db->from('income');
        $this->db->join('criteria','criteria.id=income.criteria_id');
        $this->db->join('file','income.voucher_id=file.file_id');
        if(isset($start)&&($end))
        {
            $this->db->where('time >=',$start);
            $this->db->where('time <=',$end);
        }
        if($limit!=0)
            $this->db->limit($limit,$index);
        $this->db->order_by('time','desc');
        $this->db->order_by('income_id','desc');
        $query=$this->db->get();
        return $query->result_array();
    }
    public function addExpensesInfo($VoucherData)
    {

        $this->db->insert('file',$VoucherData);

        $this->db->select('*');
        $this->db->from('file');
        $this->db->limit(1);
        $this->db->order_by('file_id','desc');
        $query=$this->db->get();

        $File=$query->row();

        $SessionData=$this->session->all_userdata();

        $data=array(
            'criteria_id'=>$this->input->post('criteria_id'),
            'description'=>$this->input->post('description'),
            'time'=>$this->input->post('date_of_expense'),
            'financial_year'=>$this->input->post('financial_year'),
            'amount'=>$this->input->post('amount'),
            'receiver_name'=>$this->input->post('receiver_name'),
            'approver_name'=>$this->input->post('approver_name'),
            'voucher_id'=>$File->file_id,
            'modified_id'=>$SessionData['userid']
        );
        $this->db->insert('expense',$data);

        $this->db->select('*');
        $this->db->from('expense');
        $this->db->limit(1);
        $this->db->order_by('expense_id','desc');
        $query=$this->db->get();
        $ExpensesData=$query->row();
        return $ExpensesData->expense_id;
    }
    public function updateExpensesInfo($ExpenseId,$VoucherData)
    {

        if($VoucherData!=NULL)
        {
            $this->db->select('*');
            $this->db->from('expense');
            $this->db->limit(1);
            $this->db->where('expense_id',$ExpenseId);
            $query=$this->db->get();
            $deleteData=$query->row();

            $this->db->where('file_id',$deleteData->voucher_id);
            $this->db->update('file',$VoucherData);
        }

        $SessionData=$this->session->all_userdata();

        $data=array(
            'criteria_id'=>$this->input->post('criteria_id'),
            'description'=>$this->input->post('description'),
            'time'=>$this->input->post('date_of_expense'),
            'financial_year'=>$this->input->post('financial_year'),
            'amount'=>$this->input->post('amount'),
            'receiver_name'=>$this->input->post('receiver_name'),
            'approver_name'=>$this->input->post('approver_name'),
            'modified_id'=>$SessionData['userid']
        );
        $this->db->where('expense_id',$ExpenseId);
        $this->db->update('expense',$data);

    }
    public function getExpense($ExpenseId)
    {
        $this->db->select('*');
        $this->db->from('expense');
        $this->db->join('file','expense.voucher_id=file.file_id');
        $this->db->where(array('expense.expense_id'=>$ExpenseId));
        $this->db->order_by('expense_id','desc');
        $query=$this->db->get();
        return $query->row();
    }
    public function deleteExpenseId($ExpenseId)
    {
        $this->db->select('*');
        $this->db->from('expense');
        $this->db->limit(1);
        $this->db->where('expense_id',$ExpenseId);
        $query=$this->db->get();
        $deleteData=$query->row();

        $this->db->where('file_id',$deleteData->voucher_id);
        $this->db->delete('file');

        $this->db->where('expense_id',$ExpenseId);
        $this->db->delete('expense');
    }
    public function viewAllExpense($start, $end, $index=0, $limit=0 )
    {
        $this->db->select('expense.expense_id,criteria.id,criteria.name, expense.description,expense.amount,file_path,number,voucher_name, expense.receiver_name, expense.time');

        $this->db->from('expense');
        $this->db->join('criteria','criteria.id=expense.criteria_id');
        $this->db->join('file','expense.voucher_id=file.file_id');
        if(isset($start)&&($end))
        {
            $this->db->where('time >=',$start);
            $this->db->where('time <=',$end);
        }
        if($limit!=0)
            $this->db->limit($limit,$index);
        $this->db->order_by('time','desc');
        $this->db->order_by('expense_id','desc');
        $query=$this->db->get();
        return $query->result_array();
    }
    public function Report($start, $end, $type='income', $criteria_id=0, $index=0, $limit=0)
    {
        if($type=='expense')
        {
            $this->db->select('expense.expense_id,criteria.id,criteria.name, expense.description,expense.amount,file_path,number,voucher_name, expense.receiver_name, expense.time');
            $this->db->from('expense');
            $this->db->join('criteria','criteria.id=expense.criteria_id');
            $this->db->join('file','expense.voucher_id=file.file_id');
        }
        if($type=='income')
        {
            $this->db->select('income.income_id,criteria.id,criteria.name, income.description,income.amount,file_path,number,voucher_name, income.receiver_name, income.time');
            $this->db->from('income');
            $this->db->join('criteria','criteria.id=income.criteria_id');
            $this->db->join('file','income.voucher_id=file.file_id');
        }


        if(isset($start)&&($end))
        {
            $this->db->where('time >=',$start);
            $this->db->where('time <=',$end);
        }
        if($criteria_id!=0)
            $this->db->where('criteria_id',$criteria_id);
        if($limit!=0)
            $this->db->limit($limit,$index);
        $this->db->order_by('time','desc');
        //$this->db->order_by('modification_time','desc');
        $query=$this->db->get();
        return $query->result_array();

    }

}
?>
<?php
class Userdb extends CI_Model {

    public function __construct()
    {

    }

    public function loginCheck()
    {
        $email=$this->input->post('username');
        $email=mysql_real_escape_string($email);
        $password=$this->input->post('password');

        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email',$email);
        $this->db->or_where('contact_number', $email);
        $this->db->where('password',$password);
        $this->db->where('is_active','1');
        $query=$this->db->get();
        //$query = $this->db->get_where('users', array('email' => $email, 'password' =>  $password,'is_active'=>1));

        if($query->row_array()==null)
        {
            return false;
        }
        else
        {
            $user_data=$query->row();
            $role=$user_data->role;
            $session_data=array('logged_in'=>true,'userid'=>$user_data->user_id,'email'=>$email, 'role_id'=>$role,'first_name'=>$user_data->first_name,'last_name'=>$user_data->last_name);
            $this->session->set_userdata($session_data);
            return true;
        }

    }
    public function getUserInformationByID($user_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('user_id',$user_id);
        $this->db->join('user_role','users.role=user_role.role_id');
        $this->db->limit(1);
        $this->db->order_by('user_id','desc');
        $query=$this->db->get();
        return $query->row();
    }

    public function getUserInformationByEmail($email)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('user_role','users.role=user_role.role_id');
        $this->db->where('email',$email);
        $this->db->or_where('contact_number',$email);
        $this->db->limit(1);
        $this->db->order_by('user_id','desc');
        $query=$this->db->get();
        return $query->row();
    }
    public function updatePasswordByEmail($email,$password)
    {
        $data=array(
            'password'=>$password
        );
        $this->db->where('email',$email);
        $this->db->update('users',$data);
    }
    public function existingCheck($email)
    {
        $query = $this->db->get_where('users', array('email' => $email,'is_active'=>1));

        if($query->row_array()==null)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

}
?>
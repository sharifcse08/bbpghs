<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator extends  CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->library('upload');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('table');
        $this->load->library('form_validation');
        $this->load->library('email');
        include_once('phpToPDF.php') ;

    }
    public function loginCheck()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $session_data=$this->session->all_userdata();
        if(!isset($session_data['role_id']))
        {
            redirect('login');
        }
        return $session_data['role_id'];
    }
    public function index()
    {
        $this->loginCheck();

        $data['user_type']=$this->role_id();
        $data['menu']='criteria';
        $data['submenu']='add_criteria';
        $data['roles']=$this->admindb->getRoles();;
        $data['navbar']='includes/navbar.php';
        $data['title'] = TITLE.' >>Welcome to '. LOGOTEXT;
        $data['body'] = 'admin/index.php';
        $this->load->view('template', $data);
    }
    public function role_id()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $session_data=$this->session->all_userdata();
        return $session_data['role_id'];
    }
    public function AddCriteria()
    {
        $this->loginCheck();

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('type', 'Criteria Type', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['user_type']=$this->role_id();
            $data['menu']='criteria';
            $data['submenu']='add_criteria';
            $data['roles']=$this->admindb->getRoles();;
            $data['navbar']='includes/navbar.php';
            $data['title'] = TITLE.' >>Add New Criteria';
            $data['body'] = 'operator/add_criteria.php';
            $data['header']='নতুন খাত';

            $post=$this->input->post('post');

            if($post!="false")
                $data['message']="false";
            else
                $data['message']="";

            $this->load->view('template', $data);
        }
        else
        {
            $CriteriaData=$this->operatordb->addCriteria();
            $data['user_type']=$this->role_id();
            $data['menu']='criteria';
            $data['submenu']='';
            $data['roles']=$this->admindb->getRoles();;
            $data['navbar']='includes/navbar.php';
            $data['title'] = TITLE.' >>Update Criteria';
            $data['body'] = 'operator/update_criteria.php';
            $data['CriteriaData'] = $CriteriaData;
            $data['message']="Added Successfully !!! Please check the stored information below.";
            $data['success']=true;
            $data['header']='নতুন খাত';

            $this->load->view('template', $data);
        }
    }
    public function UpdateCriteria($Criteria_id)
    {
        $this->loginCheck();

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('type', 'Criteria Type', 'trim|required');


        if ($this->form_validation->run() == FALSE)
        {
            $data['title'] = TITLE.' >>Update New Criteria';
            $data['user_type']=$this->role_id();
            $data['menu']='criteria';
            $data['submenu']='';
            $data['roles']=$this->admindb->getRoles();;
            $data['navbar']='includes/navbar.php';
            $data['header']=OF_CRITERIA.UPDATE;


            $data['body'] = 'operator/update_criteria.php';

            $post=$this->input->post('post');

            if($post!="false")
                $data['message']="false";
            else
                $data['message']="";

            $data['CriteriaData']=$this->admindb->getCriteria($Criteria_id);

            $this->load->view('template', $data);
        }
        else
        {
            $this->operatordb->saveChangedCriteriaInfo($Criteria_id);
            $data['title'] = TITLE.' >>Update Criteria';
            $data['user_type']=$this->role_id();
            $data['menu']='criteria';
            $data['submenu']='';
            $data['roles']=$this->admindb->getRoles();;
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'operator/update_criteria.php';
            $data['CriteriaData']=$this->admindb->getCriteria($Criteria_id);
            $data['message']="Updated Successfully !!! Please check the stored information below.";
            $data['success']=true;
            $data['header']=OF_CRITERIA.UPDATE;


            //print_r($data['CriteriaData']);

            $this->load->view('template', $data);
            //redirect(base_url()."Admin/ViewUser/".$UserData->user_id."/".$data['message']."/".$data['success']);
        }
    }
    public function ViewAllCriteria()
    {
        $this->loginCheck();
        $total=$this->operatordb->viewCriteria();
        $config['base_url'] = base_url().'operator/ViewAllCriteria/';
        $config['total_rows'] =count($total);
        $config['per_page'] = '20';

        $config['num_links'] = count($total);

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        //load the model and get results

        $Criterias=$this->operatordb->viewCriteria($this->uri->segment('3'),$config['per_page']);

        $data['user_type']=$this->role_id();
        $data['menu']='criteria';
        $data['submenu']='view_criteria';
        $data['navbar']='includes/navbar.php';
        $data['title'] = TITLE.' >>View All Criteria';
        $data['body'] = 'operator/view_criterias.php';
        $data['header']="সকল খাতের বিবরণ";

        $data['Criterias']=$Criterias;
        //print_r($Users);
        $this->load->view('template', $data);
    }
    public function DeleteCriteria($Page, $CriteriaId)
    {
        $this->loginCheck();
        $this->operatordb->deleteCriteriaId($CriteriaId);
        redirect(base_url()."operator/ViewAllCriteria/".$Page."/");
    }
    public function AddIncome()
    {
        $this->loginCheck();
        $data['CriteriaData']=$this->admindb->getCriteria();

        $this->form_validation->set_rules('criteria_id', 'Criteria', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('date_of_income', 'Date of Income ', 'trim|required|regex_match[@^[0-9]{4}-[0-9]{2}-[0-9]{2}$@]');
        $this->form_validation->set_rules('amount','Amount','trim|required|numeric|greater_than[0]|less_than[9999999999999999]|max_length[20]');
        //$this->form_validation->set_rules('approver_name', 'Approver Name', 'trim|required');
        $this->form_validation->set_rules('receiver_name', 'Receiver Name', 'trim|required');
        $this->form_validation->set_rules('voucher', 'Voucher Upload', 'trim');
        $this->form_validation->set_rules('voucher_id', 'Voucher ID', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['title'] = TITLE.' >>Add Income';
            $data['user_type']=$this->role_id();
            $data['menu']='income';
            $data['submenu']='add_income';
            $data['roles']=$this->admindb->getRoles();;
            $data['users']=$this->admindb->viewUser();;
            $data['navbar']='includes/navbar.php';
            $data['header']="নতুন আয়";

            $data['body'] = 'operator/add_income.php';

            $post=$this->input->post('post');

            if($post!="false")
                $data['message']="false";
            else
                $data['message']="";

            $this->load->view('template', $data);
        }
        else
        {

            $voucher_name=NULL;
            $voucher_name_path=NULL;

            if (!empty($_FILES['voucher']['name'])) {
                // Specify configuration for File 1
                $config['upload_path'] = 'IncomeVoucher/';
                $config['allowed_types'] = 'gif|jpg|png|bmp';
                $config['max_size'] = '1000000';
                $config['max_width'] = '10000';
                $config['max_height'] = '10000';

                $this->upload->initialize($config);

                if ($this->upload->do_upload('voucher')) {
                    $data = $this->upload->data();
                    $voucher_name = $data['file_name'];
                    $voucher_name_path = "IncomeVoucher/".$data['file_name'];
                }
            }

            $VoucherData=array(
                'voucher_name'=>$voucher_name,
                'number'=>$this->input->post('voucher_id'),
                'type'=>'income',
                'file_path'=>$voucher_name_path
            );
            $data['CriteriaData']=$this->admindb->getCriteria();
            $IncomeId=$this->operatordb->addIncomeInfo($VoucherData);
            $data['title'] = TITLE.' >>Update Income';
            $data['user_type']=$this->role_id();
            $data['menu']='income';
            $data['submenu']='update_income';
            $data['roles']=$this->admindb->getRoles();;
            $data['users']=$this->admindb->viewUser();;
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'operator/update_income.php';
            $data['IncomeInfo']=$this->operatordb->getIncome($IncomeId);//print_r($data['ExpensesInfo']);
            $data['message']=INCOME_ADD_MESSAGE;
            $data['success']=true;
            $data['header']="নতুন আয়";

            $this->load->view('template', $data);
        }
    }
    public function UpdateIncome($IncomeId)
    {
        $this->loginCheck();

        $this->form_validation->set_rules('criteria_id', 'Criteria', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('date_of_income', 'Date of Income ', 'trim|required|regex_match[@^[0-9]{4}-[0-9]{2}-[0-9]{2}$@]');
        $this->form_validation->set_rules('amount','Amount','trim|required|numeric|greater_than[0]|less_than[9999999999999999]|max_length[20]');
        $this->form_validation->set_rules('receiver_name', 'Receiver Name', 'trim|required');
        $this->form_validation->set_rules('voucher', 'Voucher Upload', 'trim');
        $this->form_validation->set_rules('voucher_id', 'Voucher ID', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['CriteriaData']=$this->admindb->getCriteria();
            $data['IncomeInfo']=$this->operatordb->getIncome($IncomeId);
            $data['title'] = TITLE.' >>Update Income';;
            $data['user_type']=$this->role_id();
            $data['menu']='income';
            $data['submenu']='update_income';
            $data['roles']=$this->admindb->getRoles();;
            $data['users']=$this->admindb->viewUser();;
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'operator/update_income.php';
            $data['header']=OF_INCOME.UPDATE;

            $post=$this->input->post('post');

            if($post!="false")
                $data['message']="false";
            else
                $data['message']="";

            $this->load->view('template', $data);
        }
        else
        {

            $voucher_name=NULL;
            $voucher_name_path=NULL;

            if (!empty($_FILES['voucher']['name'])) {
                // Specify configuration for File 1
                $config['upload_path'] = 'IncomeVoucher/';
                $config['allowed_types'] = 'gif|jpg|png|bmp';
                $config['max_size'] = '1000000';
                $config['max_width'] = '10000';
                $config['max_height'] = '10000';

                $this->upload->initialize($config);

                if ($this->upload->do_upload('voucher')) {
                    $data = $this->upload->data();
                    $voucher_name = $data['file_name'];
                    $voucher_name_path = "IncomeVoucher/".$data['file_name'];
                }
            }

            if($voucher_name_path!=NULL)
            {
                $VoucherData=array(
                    'voucher_name'=>$voucher_name,
                    'number'=>$this->input->post('voucher_id'),
                    'type'=>'income',
                    'file_path'=>$voucher_name_path
                );
            }
            else
            {
                $VoucherData=array(
                    'number'=>$this->input->post('voucher_id')
                );
            }



            $this->operatordb->updateIncomeInfo($IncomeId,$VoucherData);

            $data['CriteriaData']=$this->admindb->getCriteria();
            $data['title'] = TITLE.' >>Update Income';
            $data['user_type']=$this->role_id();
            $data['menu']='income';
            $data['submenu']='update_income';
            $data['roles']=$this->admindb->getRoles();;
            $data['users']=$this->admindb->viewUser();;
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'operator/update_income.php';


            $data['IncomeInfo']=$this->operatordb->getIncome($IncomeId);//print_r($data['ExpensesInfo']);
            $data['message']=INCOME_UPDATE_MESSAGE;
            $data['success']=true;
            $data['header']=OF_INCOME.UPDATE;

            $this->load->view('template', $data);
        }
    }
    public function AddExpense()
    {
        $this->loginCheck();
        $data['CriteriaData']=$this->admindb->getCriteria();

        $this->form_validation->set_rules('criteria_id', 'Criteria', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('date_of_expense', 'Date of Expense ', 'trim|required|regex_match[@^[0-9]{4}-[0-9]{2}-[0-9]{2}$@]');
        $this->form_validation->set_rules('amount','Amount','trim|required|numeric|greater_than[0]|less_than[9999999999999999]|max_length[20]');
        $this->form_validation->set_rules('approver_name', 'Approver Name', 'trim|required');
        $this->form_validation->set_rules('receiver_name', 'Receiver Name', 'trim|required');
        $this->form_validation->set_rules('voucher', 'Voucher Upload', 'trim');
        $this->form_validation->set_rules('voucher_id', 'Voucher ID', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['title'] = TITLE.' >>Add Expenses';
            $data['user_type']=$this->role_id();
            $data['menu']='expense';
            $data['submenu']='add_expense';
            $data['roles']=$this->admindb->getRoles();;
            $data['users']=$this->admindb->viewUser();;
            $data['navbar']='includes/navbar.php';
            $data['header']="নতুন ব্যয়";

            $data['body'] = 'operator/add_expenses.php';

            $post=$this->input->post('post');

            if($post!="false")
                $data['message']="false";
            else
                $data['message']="";

            $this->load->view('template', $data);
        }
        else
        {

            $voucher_name=NULL;
            $voucher_name_path=NULL;

            if (!empty($_FILES['voucher']['name'])) {
                // Specify configuration for File 1
                $config['upload_path'] = 'ExpensesVoucher/';
                $config['allowed_types'] = 'gif|jpg|png|bmp';
                $config['max_size'] = '1000000';
                $config['max_width'] = '10000';
                $config['max_height'] = '10000';

                $this->upload->initialize($config);

                if ($this->upload->do_upload('voucher')) {
                    $data = $this->upload->data();
                    $voucher_name = $data['file_name'];
                    $voucher_name_path = "ExpensesVoucher/".$data['file_name'];
                }
            }

            $VoucherData=array(
                'voucher_name'=>$voucher_name,
                'number'=>$this->input->post('voucher_id'),
                'type'=>'expense',
                'file_path'=>$voucher_name_path
            );
            $data['CriteriaData']=$this->admindb->getCriteria();
            $ExpensesId=$this->operatordb->addExpensesInfo($VoucherData);
            $data['title'] = TITLE.' >>Update Expense';
            $data['user_type']=$this->role_id();
            $data['menu']='expense';
            $data['submenu']='update_expense';
            $data['roles']=$this->admindb->getRoles();;
            $data['users']=$this->admindb->viewUser();;
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'operator/update_expenses.php';
            $data['ExpensesInfo']=$this->operatordb->getExpense($ExpensesId);//print_r($data['ExpensesInfo']);
            $data['message']=EXPENSE_ADD_MESSAGE;
            $data['success']=true;
            $data['header']="নতুন ব্যয়";

            $this->load->view('template', $data);
        }
    }
    public function UpdateExpense($ExpenseId)
    {
        $this->loginCheck();

        $this->form_validation->set_rules('criteria_id', 'Criteria', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('date_of_expense', 'Date of Expense ', 'trim|required|regex_match[@^[0-9]{4}-[0-9]{2}-[0-9]{2}$@]');
        $this->form_validation->set_rules('amount','Amount','trim|required|numeric|greater_than[0]|less_than[9999999999999999]|max_length[20]');
        $this->form_validation->set_rules('approver_name', 'Approver Name', 'trim|required');
        $this->form_validation->set_rules('receiver_name', 'Receiver Name', 'trim|required');
        $this->form_validation->set_rules('voucher', 'Voucher Upload', 'trim');
        $this->form_validation->set_rules('voucher_id', 'Voucher ID', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['CriteriaData']=$this->admindb->getCriteria();
            $data['ExpensesInfo']=$this->operatordb->getExpense($ExpenseId);
            $data['title'] = TITLE.' >>Update Expenses';;
            $data['user_type']=$this->role_id();
            $data['menu']='expense';
            $data['submenu']='update_expense';
            $data['roles']=$this->admindb->getRoles();;
            $data['users']=$this->admindb->viewUser();;
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'operator/update_expenses.php';
            $data['header']=OF_EXPENSE.UPDATE;

            $post=$this->input->post('post');

            if($post!="false")
                $data['message']="false";
            else
                $data['message']="";

            $this->load->view('template', $data);
        }
        else
        {

            $voucher_name=NULL;
            $voucher_name_path=NULL;

            if (!empty($_FILES['voucher']['name'])) {
                // Specify configuration for File 1
                $config['upload_path'] = 'ExpensesVoucher/';
                $config['allowed_types'] = 'gif|jpg|png|bmp';
                $config['max_size'] = '1000000';
                $config['max_width'] = '10000';
                $config['max_height'] = '10000';

                $this->upload->initialize($config);

                if ($this->upload->do_upload('voucher')) {
                    $data = $this->upload->data();
                    $voucher_name = $data['file_name'];
                    $voucher_name_path = "ExpensesVoucher/".$data['file_name'];
                }
            }

            if($voucher_name_path!=NULL)
            {
                $VoucherData=array(
                    'voucher_name'=>$voucher_name,
                    'number'=>$this->input->post('voucher_id'),
                    'type'=>'expense',
                    'file_path'=>$voucher_name_path
                );
            }
            else
            {
                $VoucherData=array(
                    'number'=>$this->input->post('voucher_id')
                );
            }

            $this->operatordb->updateExpensesInfo($ExpenseId,$VoucherData);

            $data['CriteriaData']=$this->admindb->getCriteria();
            $data['title'] = TITLE.' >>Update Expenses';
            $data['user_type']=$this->role_id();
            $data['menu']='expense';
            $data['submenu']='update_expense';
            $data['roles']=$this->admindb->getRoles();;
            $data['users']=$this->admindb->viewUser();;
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'operator/update_expenses.php';

            $data['ExpensesInfo']=$this->operatordb->getExpense($ExpenseId);//print_r($data['ExpensesInfo']);
            $data['message']=EXPENSE_UPDATE_MESSAGE;
            $data['success']=true;
            $data['header']=OF_EXPENSE.UPDATE;

            $this->load->view('template', $data);
        }
    }
    public function ViewAllIncome()
    {
        $this->loginCheck();

        $start=trim($this->input->get('start',TRUE));
        $end=trim($this->input->get('end',TRUE));
        $uri_segment=$this->input->get('page',TRUE);
        $message=$this->input->get('message',TRUE);

        $year=date('Y');
        if($start=="")
        {
            if(date('m')<=6)
                $startY=$year-1;
            else
                $startY=$year;
        }

        if($end=="")
        {
            if(date('m')>6)
                $endY=$year+1;
            else
                $endY=$year;
        }

        if(empty($start))
            $start=$startY."-07-01";
        if(empty($end))
            $end= $endY."-06-30";

        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'page';

        $data['user_type']=$this->role_id();
        $total=$this->operatordb->viewAllIncome($start,$end);

        $config['base_url'] = base_url().'operator/ViewAllIncome/?start='.$start.'&end='.$end;
        $config['total_rows'] =count($total);
        $config['per_page'] = '20';

        $config['num_links'] = count($total);

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        //load the model and get results

        $Incomes=$this->operatordb->viewAllIncome($start, $end, $uri_segment,$config['per_page']);

        $data['user_type']=$this->role_id();
        $data['menu']='income';
        $data['submenu']='view_income';
        $data['navbar']='includes/navbar.php';
        $data['title'] = TITLE.' >> View Income';
        $data['body'] = 'operator/view_incomes.php';
        $data['message']=$message;

        $data['incomes']=$Incomes;
        $data['header']="সকল আয়ের বিবরন";
        //print_r($Users);
        $this->load->view('template', $data);
    }
    public function DeleteIncome()
    {
        $this->loginCheck();
        $flag=$this->operatordb->deleteIncomeId($_GET['DeleteIncomeId']);
        redirect(base_url().'operator/viewAllIncome/?start='.$_GET['start']."&end=".$_GET['end']."&page=".$_GET['page']."&message=true");
    }
    public function ViewAllExpense()
    {
        $this->loginCheck();

        $start=trim($this->input->get('start',TRUE));
        $end=trim($this->input->get('end',TRUE));
        $uri_segment=$this->input->get('page',TRUE);
        $message=$this->input->get('message',TRUE);

        $year=date('Y');
        if($start=="")
        {
            if(date('m')<=6)
                $startY=$year-1;
            else
                $startY=$year;
        }


        if($end=="")
        {
            if(date('m')>6)
                $endY=$year+1;
            else
                $endY=$year;
        }




        if(empty($start))
            $start=$startY."-07-01";
        if(empty($end))
            $end= $endY."-06-30";


        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'page';

        $data['user_type']=$this->role_id();
        $total=$this->operatordb->viewAllExpense($start,$end);

        $config['base_url'] = base_url().'operator/ViewAllExpense/?start='.$start.'&end='.$end;
        $config['total_rows'] =count($total);
        $config['per_page'] = '20';

        $config['num_links'] = count($total);

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        //load the model and get results

        $Expenses=$this->operatordb->viewAllExpense($start, $end, $uri_segment,$config['per_page']);

        $data['user_type']=$this->role_id();
        $data['menu']='expense';
        $data['submenu']='view_expense';
        $data['navbar']='includes/navbar.php';
        $data['title'] = TITLE.' >> View Expense';
        $data['body'] = 'operator/view_expenses.php';
        $data['message']=$message;
        $data['header']="সকল ব্যয়ের বিবরন";


        $data['Expenses']=$Expenses;
        //print_r($Users);
        $this->load->view('template', $data);
    }
    public function DeleteExpense()
    {
        $this->loginCheck();
        $flag=$this->operatordb->deleteExpenseId($_GET['DeleteExpenseId']);
        redirect(base_url().'operator/viewAllExpense/?start='.$_GET['start']."&end=".$_GET['end']."&page=".$_GET['page']."&message=true");
    }
    public function Report()
    {
        $start=trim($this->input->get('start',TRUE));
        $end=trim($this->input->get('end',TRUE));
        $type=trim($this->input->get('type',TRUE));
        $criteria_id=trim($this->input->get('criteria_id',TRUE));
        $uri_segment=$this->input->get('page',TRUE);



        $year=date('Y');

        if(empty($start))
            $start=($year-1)."-07-01";
        if(empty($end))
            $end= $year."-06-30";
        if($type=="")
            $type="income";
        $total=$this->operatordb->Report($start,$end,$type,$criteria_id);

        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['base_url'] = base_url().'operator/Report/?start='.$start.'&end='.$end.'&type='.$type.'&criteria_id='.$criteria_id;
        $config['total_rows'] =count($total);
        $config['per_page'] = count($total);

        $config['num_links'] = count($total);

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        //load the model and get results
        $data['user_type']=$this->role_id();

        $data['title'] = TITLE.' >>REPORT';
        $data['navbar']='includes/navbar.php';
        $data['criterias']=$this->operatordb->getCriteriaByType($type);
        if($type=='income')
        {
            $data['body'] = 'operator/income_filtered_result.php';
            $data['incomes']=$this->operatordb->Report($start,$end,$type,$criteria_id,$uri_segment, $config['per_page']);
            $data['header']='আয়ের  প্রতিবেদন';
        }
        else if($type=='expense')
        {
            $data['body'] = 'operator/expense_filtered_result.php';
            $data['expenses']=$this->operatordb->Report($start,$end,$type,$criteria_id,$uri_segment, $config['per_page']);
            $data['header']='ব্যয়ের  প্রতিবেদন';
        }


        $this->load->view('template', $data);
    }
    public function getCriteriaByType()
    {
        $type=$this->input->post('type');

        $data['criterias']=$this->operatordb->getCriteriaByType($type);
        echo json_encode($data);
    }
    public function IncomeReport($year1="",$year2="")
    {
        $this->loginCheck();

        $year=$this->input->post('financial_year');

        if($year!="")
        {
            if($year1=="")
                $year1=$year-1;
            if($year2=="")
                $year2=$year1+1;
        }

        else
        {
            $year=date('Y');
            if($year1=="")
            {
                if(date('m')<=6)
                    $year1=$year-1;
                else
                    $year1=$year;

            }

            if($year2=="")
            {
                if(date('m')>6)
                    $year2=$year+1;
                else
                    $year2=$year;
            }
        }



        $data['user_type']=$this->role_id();
        $data['menu']='report';
        $data['submenu']='income_report';

        $data['title'] = TITLE.' >>Income Report';
        $data['navbar']='includes/navbar.php';
        $data['body'] = 'admin/income_report.php';
        $data['year']=$year2;
        $data["MonthData"]=$this->admindb->incomeReport($year1,$year2);
        $data['header']='আয়ের বার্ষিক প্রতিবেদন';

        $this->load->view('template', $data);
    }
    public function ExpenseReport($year1='',$year2='')
    {
        $this->loginCheck();

        $year=$this->input->post('financial_year');

        if($year!="")
        {
            if($year1=="")
                $year1=$year-1;
            if($year2=="")
                $year2=$year1+1;
        }

        else
        {
            $year=date('Y');
            if($year1=="")
            {
                if(date('m')<=6)
                    $year1=$year-1;
                else
                    $year1=$year;

            }

            if($year2=="")
            {
                if(date('m')>6)
                    $year2=$year+1;
                else
                    $year2=$year;
            }
        }


        $data['user_type']=$this->role_id();
        $data['menu']='report';
        $data['submenu']='expense_report';

        $data['title'] = TITLE.' >>Expense Report';
        $data['navbar']='includes/navbar.php';
        $data['body'] = 'admin/expense_report.php';
        $data['year']=$year2;
        $data["MonthData"]=$this->admindb->expenseReport($year1,$year2);
        $data['header']='ব্যয়ের বার্ষিক  প্রতিবেদন';

        $this->load->view('template', $data);
    }
    public function CreateBackup()
    {
        $this->loginCheck();
        $data['title'] = TITLE.' >>Create Backup';
        $data['navbar']='includes/navbar.php';
        $data['body'] = 'operator/backup.php';
        $data['header']='সংরক্ষণ করুন';

        $this->load->view('template', $data);
    }
    public function UploadBackup()
    {
        $data['title'] = TITLE.' >>Upload Backup';
        $data['navbar']='includes/navbar.php';
        $data['body'] = 'operator/upload_backup.php';
        $data['header']='রিস্টোর করুন';
        $this->load->view('template', $data);
    }
    public function RestoreDB()
    {
        $this->loginCheck();

        $filename=$_FILES["sqlfile"]["tmp_name"];


        // Connect to MySQL server
        $conn=mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD) or die('Error connecting to MySQL server: ' . mysql_error());
        // Select database
        mysqli_select_db($conn,DB_NAME) or die('Error selecting MySQL database: ' . mysql_error());

        // Temporary variable, used to store current query
        $templine = '';
        // Read in entire file
        $lines = file($filename);
        // Loop through each line
        foreach ($lines as $line)
        {
            // Skip it if it's a comment
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;

            // Add this line to the current segment
            $templine .= $line;
            // If it has a semicolon at the end, it's the end of the query
            if (substr(trim($line), -1, 1) == ';')
            {
                // Perform the query
                mysqli_query($conn,$templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
                // Reset temp variable to empty
                $templine = '';
            }
        }

        $data['title'] = TITLE.' >>Successful Backup';
        $data['navbar']='includes/navbar.php';
        $data['body'] = 'operator/backup_successful.php';
        $data['header']='সংরক্ষণ করুন';
        $this->load->view('template', $data);

    }
    public function PartialReport($start,$end,$type,$criteria_id=0)
    {

        if($type=="income")
        {
            $reportType=OF_INCOME.REPORT;
            $reports=$this->operatordb->Report($start,$end,$type,$criteria_id,0, 0);
        }

        else
        {
            $reportType=OF_EXPENSE.REPORT;
            $reports=$this->operatordb->Report($start,$end,$type,$criteria_id,0, 0);
        }


        $thead='
                <tr>
                            <th>#</th>
                            <th>খাত</th>
                            <th>বিবরণ</th>
                            <th>টাকার পরিমাণ</th>
                            <th>তারিখ</th>
                            <th>আদায়কারীর নাম</th>
                            <th>রশিদ নং</th>
                </tr>';


        $tbody="";


        $i=1;
        foreach($reports as $report){

            $tbody.='<tr>';
            $tbody.='<td>';
            $tbody.=$i;
            $tbody.='</td>';

            $tbody.='<td>';
            $tbody.=$report['name'];
            $tbody.='</td>';

            $tbody.='<td>';
            $tbody.=$report['description'];
            $tbody.='</td>';

            $tbody.='<td>';
            $tbody.=$report['amount'];
            $tbody.='</td>';

            $tbody.='<td>';
            $tbody.=$report['time'];
            $tbody.='</td>';

            $tbody.='<td>';
            $tbody.=$report['receiver_name'];
            $tbody.='</td>';

            $tbody.='<td>';
            $tbody.=$report['number'];
            $tbody.='</td>';

            $tbody.='</tr>';
            $i++;
        }
        $tbody.='<tr ><td colspan="13" style="text-align: center;"> তারিখের ব্যবধান : '. $start.'  থেকে   '.($end).'</td></tr>';



        $html = '<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
    <title>'.TITLE.' - 2014</title>
    <style>
        body {
            color: #333333;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 1.42857;
        }
        h5, .h5 {
            font-size: 14px;
        }
        h4, h5, h6 {
            margin-bottom: 10px;
            margin-top: 10px;
        }
        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 500;
            line-height: 1.1;
        }
        *, *:before, *:after {
            -moz-box-sizing: border-box;
        }
        *, *:before, *:after {
            -moz-box-sizing: border-box;
        }
        .table-bordered {
            border: 1px solid #DDDDDD;
        }
        .table {
            margin-bottom: 20px;
            width: 100%;
        }
        table {
            background-color: rgba(0, 0, 0, 0);
            max-width: 100%;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
        *, *:before, *:after {
            -moz-box-sizing: border-box;
        }
        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #DDDDDD;
        }
        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
            border-top: 1px solid #DDDDDD;
            line-height: 1.42857;
            padding: 8px;
            vertical-align: top;
        }
    </style>

</head>
<div style="text-align:center;">
    <h2>'.TITLE.'</h2>
    <h5>'.$reportType.'</h5>
</div>


<table class="table table-bordered" border="1">
    <thead>'.
            $thead
            .'</thead>
    <tbody>'.
            $tbody
            .'</tbody>
                 </table>


</html>';

        echo $html;


        //$file_name=$pdfType.'_'.date('y').'.pdf';
        // echo "<a target='_blank' href='".base_url()."PDF/".$file_name."' > Download Now</a> &nbsp;&nbsp;&nbsp;";

        //phptopdf_html($html,'PDF/', $file_name);
    }
    public function getPDF($type,$year)
    {
        $this->loginCheck();
        $pdfType=$type;
        if($type=='income')
            $reportType=INCOME;
        else
            $reportType=EXPENSE;


        if($year=="")
            $year=date('Y');


        $year1=$year-1;

        $year2=$year1+1;
        if($type=="income")
            $MonthData=$this->admindb->incomeReport($year1,$year2);
        else
            $MonthData=$this->admindb->expenseReport($year1,$year2);

        $thead='
               <tr>
                  <th>#</th>
                  <th>JULY</th>
                  <th>AUGUST</th>
                  <th>SEPTEMBER</th>
                  <th>OCTOBER</th>
                  <th>NOVEMBER</th>
                  <th>DECEMBER</th>
                  <th>JANUARY</th>
                  <th>FEBRUARY</th>
                  <th>MARCH</th>
                  <th>APRIL</th>
                  <th>MAY</th>
                  <th>JUNE</th>
                  <th>TOTAL</th>
               </tr>';


$tbody="";


$TotalSum=0;
        foreach($MonthData as $Data){
            $tbody.='<tr>';
            foreach($Data as $month){
                $tbody.='<td>'.$month.' </td>';
            }

                $tbody.='<td>';

                $sum=0;
                for($i=1;$i<=12;$i++)
                {
                    $sum+=$Data[$i];
                }
                $TotalSum+=$sum;
                $tbody.=$sum;

                $tbody.='</td>';


            $tbody.='</tr>';
        }
$tbody.='<tr ><td colspan="13" style="text-align: center;">অর্থ বছর : '. ($year-1).'--'.($year).'</td> <td>'.$TotalSum.'</td></tr>';



$html = '<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
    <title>'.TITLE.' - 2014</title>
    <style>
        body {
            color: #333333;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 14px;
            line-height: 1.42857;
        }
        h5, .h5 {
            font-size: 14px;
        }
        h4, h5, h6 {
            margin-bottom: 10px;
            margin-top: 10px;
        }
        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 500;
            line-height: 1.1;
        }
        *, *:before, *:after {
            -moz-box-sizing: border-box;
        }
        *, *:before, *:after {
            -moz-box-sizing: border-box;
        }
        .table-bordered {
            border: 1px solid #DDDDDD;
        }
        .table {
            margin-bottom: 20px;
            width: 100%;
        }
        table {
            background-color: rgba(0, 0, 0, 0);
            max-width: 100%;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
        *, *:before, *:after {
            -moz-box-sizing: border-box;
        }
        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #DDDDDD;
        }
        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
            border-top: 1px solid #DDDDDD;
            line-height: 1.42857;
            padding: 8px;
            vertical-align: top;
        }
    </style>

</head>
<div style="text-align:center;">
    <h2>'.TITLE.'</h2>
    <h5>'.TYPE.' : '. $reportType.'</h5>
</div>


<table class="table table-bordered" border="1">
    <thead>'.
    $thead
    .'</thead>
    <tbody>'.
    $tbody
    .'</tbody>
                 </table>


</html>';

        echo $html;


        //$file_name=$pdfType.'_'.date('y').'.pdf';
      // echo "<a target='_blank' href='".base_url()."PDF/".$file_name."' > Download Now</a> &nbsp;&nbsp;&nbsp;";

       //phptopdf_html($html,'PDF/', $file_name);
    }
    public function email_check($email)
    {
        $flag=$this->userDB->existingCheck($email);
        echo $flag;
        if($flag==true)
        {
            return $flag;
        }
        else
        {
            $this->form_validation->set_message('email_check', 'No user for this Email.');
            return $flag;
        }

    }
    }

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
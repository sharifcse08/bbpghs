<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Admin extends  CI_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->library('upload');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('table');
        $this->load->library('form_validation');
        $this->load->library('email');

    }
	public function index()
	{
        $this->loginCheck();
        $data['user_type']=$this->role_id();
        $data['title'] = TITLE.' >>Sign in';
        $data['navbar']='includes/navbar.php';
        $data['body'] = 'admin/index.php';
        $this->load->view('template', $data);
	}
    public function loginCheck()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $session_data=$this->session->all_userdata();
        if($session_data['role_id']=="")
        {
            redirect('login');
        }
        return $session_data['role_id'];
    }
    public function role_id()
    {
        if(!isset($_SESSION)){
            session_start();
        }
        $session_data=$this->session->all_userdata();
        return $session_data['role_id'];
    }
    public function Create()
    {
        $this->loginCheck();
        $data['user_type']=$this->role_id();
        $data['menu']='user';
        $data['submenu']='create_user';
        $data['roles']=$this->admindb->getRoles();
        $data['title'] = TITLE.' >>Create new User';
        $data['navbar']='includes/navbar.php';
        $data['body'] = 'admin/create_user.php';
        $data['message']=false;
        $data['header']="নতুন ব্যবহারকারী";
        $this->load->view('template', $data);
    }
    public function CreateUser()
    {
        $this->loginCheck();

        $data['roles']=$this->admindb->getRoles();

        $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[cpassword]');
        $this->form_validation->set_rules('cpassword', 'Password Confirmation', 'trim|required');
        $this->form_validation->set_rules('phone', 'Contact Number', 'trim|numeric|integer|min_length[11]|is_unique[users.contact_number]');



        if ($this->form_validation->run() == FALSE)
        {
            $data['user_type']=$this->role_id();
            $data['menu']='user';
            $data['submenu']='create_user';
            $data['title'] = TITLE.' >>Create new User';
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'admin/create_user.php';
            $data['message']="validate_error";
            $data['header']="নতুন ব্যবহারকারী";
            $this->load->view('template', $data);
        }
        else
        {
            $UserData=$this->admindb->saveUser();
            $data['user_type']=$this->role_id();
            $data['menu']='user';
            $data['submenu']='';
            $data['title'] = TITLE.' >>Update  User';
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'admin/edit_user.php';
            $data['userInfo']=$UserData;
            $data['message']=USER_ADD_MESSAGE;
            $data['success']=true;
            $data['header']="নতুন ব্যবহারকারী";
            $this->load->view('template', $data);

            //redirect(base_url()."Admin/ViewUser/".$UserData->user_id."/".$data['message']);
        }

    }
    public function ViewAllUser()
    {
        $this->loginCheck();
        $total_users=$this->admindb->viewUser();
        $config['base_url'] = base_url().'admin/ViewAllUser/';
        $config['total_rows'] =count($total_users);
        $config['per_page'] = '10';

        $config['num_links'] = count($total_users);

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        //load the model and get results

        $Users=$this->admindb->viewAllUser($this->uri->segment('3'),$config['per_page']);

        $data['user_type']=$this->role_id();
        $data['menu']='user';
        $data['submenu']='view_all_user';
        $data['navbar']='includes/navbar.php';
        $data['title'] = TITLE.' >>View All Users';
        $data['body'] = 'admin/view_all_user.php';
        $data['header']='সকল ব্যবহারকারীর তথ্য ';

        $data['users']=$Users;
        //print_r($Users);
        $this->load->view('template', $data);
    }
    public function ViewUser($UserId,$message="",$success=false)
    {
        $this->loginCheck();
        $data['user_type']=$this->role_id();
        $data['menu']='user';
        $data['submenu']='';
        $data['title'] = TITLE.' >>Update  User';
        $data['navbar']='includes/navbar.php';
        $data['body'] = 'admin/edit_user.php';
        $data['message']=$message;
        $data['success']=$success;
        $data['userInfo']=$this->admindb->viewUser($UserId);

        $this->load->view('template', $data);
    }
    public function Edit($UserId)
    {
        $this->loginCheck();
        $data['roles']=$this->admindb->getRoles();

        $data['user_type']=$this->role_id();
        $data['menu']='user';
        $data['submenu']='';
        $data['title'] = TITLE.' >>Update  User';
        $data['navbar']='includes/navbar.php';
        $data['body'] = 'admin/edit_user.php';
        $data['message']=false;
        $data['userInfo']=$this->admindb->viewUser($UserId);

        $this->load->view('template', $data);
    }
    public function EditUser($UserId)
    {
        $this->loginCheck();
        $data['roles']=$this->admindb->getRoles();

        $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        //$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[cpassword]');
        $this->form_validation->set_rules('cpassword', 'Password Confirmation', 'trim|required');
        $this->form_validation->set_rules('phone', 'Contact Number', 'trim|numeric|integer|min_length[11]|callback_validate_phone_number');

        $this->form_validation->set_message('validate_phone_number','Phone number already used by another user!');

        if ($this->form_validation->run() == FALSE)
        {
            $data['user_type']=$this->role_id();
            $data['menu']='user';
            $data['submenu']='';
            $data['title'] = TITLE.' >>Update  User';
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'admin/edit_user.php';
            $data['message']="validate_error";
            $data['userInfo']=$this->admindb->viewUser($UserId);
            $data['header']='ব্যবহারকারীর '.UPDATE;
            $this->load->view('template', $data);
        }
        else
        {
            $UserData=$this->admindb->saveChangedInfo($UserId);
            $data['user_type']=$this->role_id();
            $data['menu']='user';
            $data['submenu']='';
            $data['title'] = TITLE.' >>Update  User';
            $data['navbar']='includes/navbar.php';
            $data['body'] = 'admin/edit_user.php';
            $data['userInfo']=$this->admindb->viewUser($UserId);
            $data['message']=USER_UPDATE_MESSAGE;
            $data['success']=true;
            $data['header']='ব্যবহারকারীর '.UPDATE;

            $this->load->view('template', $data);
            //redirect(base_url()."Admin/ViewUser/".$UserData->user_id."/".$data['message']."/".$data['success']);
        }
    }
    public function MakeInactiveUser($Page,$UserId)
    {
        $this->loginCheck();
        $this->admindb->MakeInactiveUser($UserId);
        redirect(base_url()."admin/ViewAllUser/".$Page."/");
    }
    public function MakeActiveUser($Page, $UserId)
    {
        $this->loginCheck();
        $this->admindb->MakeActiveUser($UserId);
        redirect(base_url()."admin/ViewAllUser/".$Page."/");
    }

    function validate_phone_number($number)
    {
        $user_id=$this->input->post('user_id');
        if(count($this->admindb->validate_phone_number($number,$user_id))>0)
        //if(true)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->library('pagination');
        $this->load->library('upload');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('table');
        $this->load->library('form_validation');
        $this->load->library('email');

    }
	public function index()
	{
		$this->load->view('welcome_message');
	}

    public function homepage()
    {
        $this->load->view('homepage');
    }
    public function date()
    {
        $this->load->view('date4');
    }
    public function getCriteriaByType()
    {
        echo json_encode(1);
    }
    public function backup()
    {
        $this->load->view('backup');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
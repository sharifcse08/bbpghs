<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/*Project used varibles*/

define('TITLE', "বুরুজ বাগান পাইলট বালিকা বিদ্যালয়");
define('LOGOTEXT', "বুরুজ বাগান পাইলট বালিকা বিদ্যালয়");
define('WELCOMETEXT', "হিসাবরক্ষণ সিস্টেমে প্রবেশ করুন");
define('COPYRIGHT', "Developed by Dhrubok Infotech Services (Pvt.) Ltd.");
define('SIGNIN',"প্রবেশ");
define('SIGNIN2',"প্রবেশ করুন");
define('SIGNOUT',"প্রস্থান");
define('USERNAME','ইমেল বা মোবাইল নং');

define('CREATE_NEW_UESR',"নতুন ব্যবহারকারি অ্যাকাউন্ট");
define('VIEW_ALL_USERS','সকল ব্যবহারকারির তথ্যাদি ');
define('UPDATE_USER','ব্যবহারকারি সম্পাদন');
define('FIRST_NAME','নামের প্রথম অংশ');
define('LAST_NAME','নামের শেষ অংশ ');
define('EMAIL',' ইমেইল');
define('PASSWORD','পাসওয়ার্ড ');
define('CONFIRM_PASSWORD','পাসওয়ার্ড নিশ্চিত করুন');
define('ROLE',' ব্যবহারকারির ভূমিকা');
define('DESIGNATION','পদমর্যাদা');
define('GENDER','ব্যবহারকারি একজন (পুরুষ / মহিলা)');
define('MOBILE','মোবাইল নাম্বার ');
define('ADDRESS',' ঠিকানা ');
define('SUBMIT','তথ্য সাবমিট করুন ');
define('UPDATE','তথ্য আপডেট করুন ');
define('USER_UPDATE','তথ্য পরিবর্তন');
define('CRITERIA','খাত');
define('ADD_CRITERIA','নতুন খাত যোগ করুন ');
define('VIEW_CRITERIA','খাতসমূহের তথ্য দেখুন ');

define('OF_CRITERIA','খাতের ');
define('OF_INCOME','আয়ের ');
define('OF_EXPENSE','ব্যয়ের ');

define('NAME','নাম ');
define('TYPE','প্রকার ');
define('DESCRIPTION','বিবরণ ');
define('EDIT','সম্পাদনা করুন');
define('DELETE','মুছে ফেলুন');
define('BLOCK','ব্লক');
define('UNBLOCK','আনব্লক');
define('INCOME','আয়সমূহ ');
define('EXPENSE','ব্যয়সমূহ ');
define('ADD_NEW_INCOME',' নতুন আয় যোগ করুন ');
define('DATE','তারিখ ');
define('FINANCIAL_YEAR','অর্থবছর');
define('AMOUNT',' টাকার পরিমাণ ');
define('RECEIVER_NAME','আদায়কারীর নাম ');
define('VOUCHER_NO',' রসিদ নং ');
define('VOUCHER_PICTURE','রসিদ সংযুক্ত করুন');
define('VIEW_VOUCHER_PICTURE','রসিদ দেখুন');
define('UPLOAD_INCOME_VOUCHER','আয়ের রসিদ থাকলে আপলোড করুন ');
define('ADD_NEW_EXPENSE',' নতুন ব্যয়ের তথ্য যোগ করুন ');
define('VIEW_EXPENSES','ব্যায়ের তথ্যাদি দেখুন ');
define('VIEW_INCOMES','আয়ের তথ্যাদি দেখুন ');
define('APPROVER_NAME','অনুমোদনকারির নাম ');
define('UPLOAD_EXPENSE_VOUCHER',' ব্যায়ের ভাউচার আপলোড করুন ');
define('INCOME_EXPENSE_REPORT','আয়/ব্যয়ের প্রতিবেদন ');
define('REPORT',' প্রতিবেদন ');
define('CREATE_REPORT','প্রতিবেদন তৈরি করুন ');

define('DESIGNATIONS','bar,foo,more,test');

define('USER_ADD_MESSAGE','ADDED');
define('USER_UPDATE_MESSAGE','UPDATED');
define('INCOME_ADD_MESSAGE','ADDED');
define('INCOME_UPDATE_MESSAGE','UPDATED');
define('EXPENSE_ADD_MESSAGE','ADDED');
define('EXPENSE_UPDATE_MESSAGE','UPDATED');


/*for database backup*/

define("DB_USER", 'root');
define("DB_PASSWORD", '');
define("DB_NAME", 'bpghs');
define("DB_HOST", 'localhost');
define("TABLES", '*');


/* End of file constants.php */
/* Location: ./application/config/constants.php */
CREATE DATABASE IF NOT EXISTS bpghs;

USE bpghs;

DROP TABLE IF EXISTS criteria;

CREATE TABLE `criteria` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO criteria VALUES("1","à¦¬à§‡à¦¤à¦¨","income","asdf");
INSERT INTO criteria VALUES("2","test3","income","testtestst");
INSERT INTO criteria VALUES("3","test","income","asdfasdf");
INSERT INTO criteria VALUES("4","test2","income","adfad");
INSERT INTO criteria VALUES("5","expense1","expense","asdfasdf");
INSERT INTO criteria VALUES("6","expense2","expense","afasd");



DROP TABLE IF EXISTS expense;

CREATE TABLE `expense` (
  `expense_id` int(15) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `time` date NOT NULL,
  `amount` float NOT NULL,
  `financial_year` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `criteria_id` int(11) NOT NULL,
  `voucher_id` int(15) DEFAULT NULL,
  `receiver_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `approver_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modified_id` int(11) DEFAULT NULL,
  `modification_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`expense_id`),
  KEY `criteria_idx` (`criteria_id`),
  KEY `user_idx` (`modified_id`),
  KEY `voucher_idx` (`voucher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO expense VALUES("1","adf","2014-05-18","34234","2013-2014","5","18","test doctor test doctor","asdf ADFASDF","1","2014-05-18 18:21:04");



DROP TABLE IF EXISTS file;

CREATE TABLE `file` (
  `file_id` int(15) NOT NULL AUTO_INCREMENT,
  `voucher_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_path` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO file VALUES("1","Capture2.PNG","income","32423","IncomeVoucher/Capture2.PNG","2014-04-28 01:57:56");
INSERT INTO file VALUES("2","ubuntu8.PNG","income","3","IncomeVoucher/ubuntu8.PNG","2014-04-28 02:02:50");
INSERT INTO file VALUES("4","BURUJ_BAGAN_PILOT_GIRLS_SCHOOL2014_--Edit_User_2014-04-22_14-07-231.png","income","3","IncomeVoucher/BURUJ_BAGAN_PILOT_GIRLS_SCHOOL2014_--Edit_User_2014-04-22_14-07-231.png","2014-04-28 02:11:19");
INSERT INTO file VALUES("7","Capture2.PNG","expense","14","ExpensesVoucher/Capture2.PNG","2014-04-28 02:39:23");
INSERT INTO file VALUES("8","1614142066-Photo_on_5-3-14_at_6.47_PM_.jpg","income","asdfasd","IncomeVoucher/1614142066-Photo_on_5-3-14_at_6.47_PM_.jpg","2014-05-18 16:33:29");
INSERT INTO file VALUES("9","1614142066-Photo_on_5-3-14_at_6.47_PM_1.jpg","income","aasdf","IncomeVoucher/1614142066-Photo_on_5-3-14_at_6.47_PM_1.jpg","2014-05-18 16:34:04");
INSERT INTO file VALUES("10","10169371_725905434118020_1952475775_n.jpg","income","asdf","IncomeVoucher/10169371_725905434118020_1952475775_n.jpg","2014-05-18 16:34:52");
INSERT INTO file VALUES("11","10169371_725905434118020_1952475775_n1.jpg","income","12312","IncomeVoucher/10169371_725905434118020_1952475775_n1.jpg","2014-05-18 16:35:45");
INSERT INTO file VALUES("12","10169371_725905434118020_1952475775_n2.jpg","income","32234","IncomeVoucher/10169371_725905434118020_1952475775_n2.jpg","2014-05-18 16:36:29");
INSERT INTO file VALUES("13","1614142066-Photo_on_5-3-14_at_6.47_PM_.jpg","expense","3423","ExpensesVoucher/1614142066-Photo_on_5-3-14_at_6.47_PM_.jpg","2014-05-18 18:21:04");
INSERT INTO file VALUES("14","10169371_725905434118020_1952475775_n3.jpg","income","asdasd","IncomeVoucher/10169371_725905434118020_1952475775_n3.jpg","2014-05-18 18:45:55");
INSERT INTO file VALUES("15","10169371_725905434118020_1952475775_n4.jpg","income","asdfa","IncomeVoucher/10169371_725905434118020_1952475775_n4.jpg","2014-05-18 18:47:50");
INSERT INTO file VALUES("16","10169371_725905434118020_1952475775_n5.jpg","income","adf","IncomeVoucher/10169371_725905434118020_1952475775_n5.jpg","2014-05-18 18:49:17");
INSERT INTO file VALUES("17","10169371_725905434118020_1952475775_n6.jpg","income","233","IncomeVoucher/10169371_725905434118020_1952475775_n6.jpg","2014-05-18 18:49:46");
INSERT INTO file VALUES("18","10169371_725905434118020_1952475775_n7.jpg","income","233","IncomeVoucher/10169371_725905434118020_1952475775_n7.jpg","2014-05-18 18:50:28");
INSERT INTO file VALUES("19","10264322_10151950580531353_1436272464894605152_n.jpg","income","asdf","IncomeVoucher/10264322_10151950580531353_1436272464894605152_n.jpg","2014-05-20 18:51:15");



DROP TABLE IF EXISTS income;

CREATE TABLE `income` (
  `income_id` int(15) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `time` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `amount` float NOT NULL,
  `financial_year` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `criteria_id` int(11) NOT NULL,
  `voucher_id` int(15) DEFAULT NULL,
  `receiver_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modified_id` int(11) DEFAULT NULL,
  `modification_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`income_id`),
  KEY `criteria_idx` (`criteria_id`),
  KEY `user_idx` (`modified_id`),
  KEY `file_idx` (`voucher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO income VALUES("1","test","2014-04-10","3234","2013-2014","1","18","test patient test patient","1","2014-04-16 01:57:57");
INSERT INTO income VALUES("4","dasdf","2014-02-04","500","2013-2014","3","10","asdfasdf","1","2014-05-18 16:34:52");
INSERT INTO income VALUES("5","fasdf","2013-12-02","300","2013-2014","3","11","asdfad","1","2014-05-18 16:35:45");
INSERT INTO income VALUES("7","asdf","2014-04-29","125544","2013-2014","1","14","Md Shariful Islam saful","1","2014-05-18 18:45:55");
INSERT INTO income VALUES("9","asdf","2014-05-09","34","2013-2014","4","16","Md Shariful Islam saful","1","2014-05-18 18:49:17");
INSERT INTO income VALUES("10","adfa","2014-05-13","3","2013-2014","3","17","Md Shariful Islam saful","1","2014-05-18 18:49:46");
INSERT INTO income VALUES("11","adfa","2014-05-13","3","2013-2014","3","18","Md Shariful Islam saful","1","2014-05-18 18:50:28");
INSERT INTO income VALUES("12","asdfasdf","2011-12-13","32","2013-2014","1","19","Md Shariful Islam saful","1","2014-05-20 18:51:15");



DROP TABLE IF EXISTS user_role;

CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(200) NOT NULL,
  `role_id` int(11) NOT NULL,
  `activity` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_role_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO user_role VALUES("1","Adminstrator","1","1");
INSERT INTO user_role VALUES("2","Teacher","2","1");
INSERT INTO user_role VALUES("3","operator","3","1");



DROP TABLE IF EXISTS users;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL DEFAULT 'male',
  `contact_number` varchar(50) NOT NULL,
  `address` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `member_since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` varchar(11) NOT NULL DEFAULT 'true',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  KEY `role` (`role`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`role`) REFERENCES `user_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO users VALUES("1","Md Shariful Islam","saful","","sharif.cse.08@gmail.com","test","male","01745192550","","1","2014-04-15 18:24:14","1");
INSERT INTO users VALUES("3","test doctor","test doctor","","doctor@doctor.com","test","male","","","2","2014-04-15 21:16:39","1");
INSERT INTO users VALUES("4","test patient","test patient","","patient@patient.com","test","female","1044444444","dfsdf","2","2014-04-15 21:16:39","0");
INSERT INTO users VALUES("5","test operat","test operat","","opterat@operat.com","test","female","","","1","2014-04-15 21:17:50","1");
INSERT INTO users VALUES("7","test","test","bar","test@test.com","test","male","14252422232","","3","2014-04-16 00:07:01","0");
INSERT INTO users VALUES("8","asdf","ADFASDF","","asdf@asda.cod","a","male","asdf","adf","2","2014-04-16 00:20:59","1");
INSERT INTO users VALUES("9","asdf","ADFASDF","","adsdf@asda.cod","a","male","","","1","2014-04-16 00:22:43","1");
INSERT INTO users VALUES("10","asdf","ADFASDF","","adfsdf@asda.cod","a","male","","","1","2014-04-16 00:25:03","1");
INSERT INTO users VALUES("11","asdf","ADFASDF","","adfsddf@asda.cod","a","male","","","1","2014-04-16 00:25:54","0");
INSERT INTO users VALUES("12","Md Shariful ISlam","tset","","sharif.dcse.08@gmail.com","asdf","male","","","1","2014-04-18 16:46:05","true");
INSERT INTO users VALUES("13","Md Shariful ISlam","asdf","","sasdharif.cse.08@gmail.com","asdf","male","","","1","2014-04-18 16:50:46","true");
INSERT INTO users VALUES("14","new","user","test2","new@new.com","test","male","234234","","1","2014-05-18 17:41:35","true");
INSERT INTO users VALUES("15","asdfasdas","dfasdf","test1","sadfasd@asdf.com","test","male","01745192550","","1","2014-05-18 18:41:15","true");
INSERT INTO users VALUES("16","à¦¹à§‡","à¦¨à¦¾à¦¹","bar","test@TEST.vom","test","male","01745192550","à¦¹à§‡à¦²à§à¦²","1","2014-05-26 00:45:37","true");




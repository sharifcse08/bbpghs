/* 
 * Script for dynamic form field add in Admin html
 * editor:nayem, date: Nov07,2013
 */
$(document).ready(function() {

        var MaxInputs = 4; //maximum input boxes allowed
        var InputsWrapper = $("#AdminWrapper"); //Input boxes wrapper ID
        var AddButton = $("#AddMoreAdmin"); //Add button ID

        var x = InputsWrapper.length; //initlal text box count
        var FieldCount = 0; //to keep track of text box added

        $(AddButton).click(function(e)  //on add input button click
        {
            if (x <= MaxInputs) //max input box allowed
            {
                FieldCount++; //text box added increment
                //add input box
                //$(InputsWrapper).append('<div><input type="text" name="mytext[]" id="field_' + FieldCount + '" value="Text ' + FieldCount + '"/><a href="#" class="removeclass">&times;</a></div>');
                $(".addition").append('<div class="form-group">'+
                '   <div class="col-lg-offset-3 col-lg-7">'+
                '       <input type="text" class="form-control" name="newAdmin[]" id="email_' + x + '" placeholder="Email Address' + FieldCount + '">'+
                '   </div> '+
                '   <a class="btn btn-link" id="removeEmail"><span class="glyphicon glyphicon-remove"></span></a>'+
                '</div>');

                x++; //text box increment
            }
            return false;
        });

        $("body").on("click", "#removeEmail", function(e) { //user click on remove text
            if (x > 1) {
                $(this).parent('div').remove(); //remove text box
                x--; //decrement textbox
            }
            return false;
        });

    });


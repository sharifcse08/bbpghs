/* 
 * Script for welcome page overlay animation
 * editor: nayem, date: nov07,2013
 */
$(document).ready(function() {
    var flag = 0;

    $('#contest_note1').hide();
    $('#contest_note2').hide();
    $('#contest_note3').hide();
    $('#contest_note4').hide();

    $("#contest_image1").mouseenter(
            function() {
                $("#contest_image1").effect("explode", {}, 500);
                // callback function to bring a hidden box back
                function callback() {
                    setTimeout(function() {
                        $("#contest_image1").removeAttr("style").hide().fadeIn();
                    }, 1000);
                }
                ;
                $('#contest_note1').fadeToggle("slow", "linear");
                //$('#contest_image1').fadeToggle("slow", "linear");
            }
    );
    $("#contest_note1").mouseleave(
            $("#contest_image1").mouseleave(function() {
        flag = 1;
    }),
            function() {
                if (flag === 1) {
                    $('#contest_note1').fadeToggle("slow", "linear");
                    $('#contest_image1').fadeToggle("slow", "linear");
                    flag = 0;
                }
            }
    );

    $("#contest_image2").mouseenter(
            function() {
                $("#contest_image2").effect("explode", {}, 500);
                $('#contest_note2').fadeToggle("slow", "linear");
                //$('#contest_image2').fadeToggle("slow", "linear");
            }
    );
    $("#contest_note2").mouseleave(
            $("#contest_image2").mouseleave(function() {
        flag = 1;
    }),
            function() {
                if (flag === 1) {
                    $('#contest_note2').fadeToggle("slow", "linear");
                    $('#contest_image2').fadeToggle("slow", "linear");
                    flag = 0;
                }
            }
    );

    $("#contest_image3").mouseenter(
            function() {
                $("#contest_image3").effect("explode", {}, 500);
                $('#contest_note3').fadeToggle("slow", "linear");
                //$('#contest_image3').fadeToggle("slow", "linear");
            }
    );
    $("#contest_note3").mouseleave(
            $("#contest_image3").mouseleave(function() {
        flag = 1;
    }),
            function() {
                if (flag === 1) {

                    $('#contest_note3').fadeToggle("slow", "linear");
                    $('#contest_image3').fadeToggle("slow", "linear");
                    flag = 0;
                }
            }
    );

    $("#contest_image4").mouseenter(
            function() {
                $("#contest_image4").effect("explode", {}, 500);
                $('#contest_note4').fadeToggle("slow", "linear");
                //$('#contest_image4').fadeToggle("slow", "linear");
            }
    );
    $("#contest_note4").mouseleave(
            $("#contest_image4").mouseleave(function() {
        flag = 1;
    }),
            function() {
                if (flag === 1) {

                    $('#contest_note4').fadeToggle("slow", "linear");
                    $('#contest_image4').fadeToggle("slow", "linear");
                    flag = 0;
                }
            }
    );
});


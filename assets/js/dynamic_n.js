/* 
 * Script for dynamic form field add in html
 * editor: nayem, date: nov07,2013
 */

$(document).ready(function() {

        var MaxInputs = 3; //maximum input boxes allowed
        var InputsWrapper = $("#InputsWrapper"); //Input boxes wrapper ID
        var AddButton = $("#AddMoreFileBox"); //Add button ID
        var profileBkash=$("#profileBkash");

        var x = InputsWrapper.length; //initlal text box count
        var FieldCount = 0; //to keep track of text box added

        $(AddButton).click(function(e)  //on add input button click
        {
            if (x <= MaxInputs) //max input box allowed
            {
                FieldCount++; //text box added increment
                //add input box
                //$(InputsWrapper).append('<div><input type="text" name="mytext[]" id="field_' + FieldCount + '" value="Text ' + FieldCount + '"/><a href="#" class="removeclass">&times;</a></div>');
                if(!document.getElementById("profileBkash")){
                $(".addition").append('<div class="form-group">'+
                '   <div class="col-lg-offset-3 col-lg-6">'+
                '       <input type="text" class="form-control" name="regBkashTransition[]" id="bkashNumber_' + x + '" placeholder="Transition Number' + FieldCount + '">'+
                '   </div> '+
                '   <a class="btn btn-link" id="removeID"><span class="glyphicon glyphicon-remove"></span></a>'+
                '   <!-- 23 character long error message -->'+
                '   <a class="text-danger"></a>'+
                '</div>');
                }
                else{
                $(".addition").append('<div class="form-group">'+
                '   <div class="col-lg-offset-3 col-lg-6">'+
                '       <input type="text" class="form-control" name="regBkashTransition[]" id="bkashNumber_' + x + '" placeholder="Transition Number' + FieldCount + '">'+
                '   </div> '+
                '   <a class="btn btn-link" id="removeID"><span class="glyphicon glyphicon-remove"></span></a>'+
                '   <!-- 11 character long error message -->'+
                '   <a class="text-danger">01234567890</a>'+
                '</div>');
                }

                x++; //text box increment
            }
            return false;
        });

        $("body").on("click", "#removeID", function(e) { //user click on remove text
            if (x > 1) {
                $(this).parent('div').remove(); //remove text box
                x--; //decrement textbox
            }
            return false;
        });

    });


